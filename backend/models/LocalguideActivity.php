<?php
namespace backend\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;

use yii\web\UploadedFile; 
 
use yii\helpers\ArrayHelper;
use frontend\models\UserForm;
use frontend\models\Personalinfo;

use frontend\models\LocalguidePostInvite;
use frontend\models\LocalguidePostInviteMsgs;
use frontend\models\Verify;
use frontend\models\Friend;
use frontend\models\Abuse;
use frontend\models\AbuseStatement;



/**
 * This is the model class for collection "occupation".
 *
 * @property \MongoId|string $_id
 * @property mixed $post_text
 * @property mixed $post_status
 * @property mixed $post_created_date
 * @property mixed $post_user_id
 * @property mixed $image
 */

class LocalguideActivity extends ActiveRecord
{

   /*public $imageFile1;*/
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'localguide_activity';
    }

   /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
         return ['_id', 'name', 'created_at', 'modify_at'];

    }

    public function addactivity($uid, $name) {
        if($uid) {
            $created_at = strtotime('now');
            $LocalguideActivity = new LocalguideActivity();
            $LocalguideActivity->name = $name;
            $LocalguideActivity->created_at = $created_at;
            if($LocalguideActivity->save()) {
                $created_at  = date("Y-m-d H:i:s", $created_at);
                $result = array('status' => true, 'name' => $name, 'created_at' => $created_at);
                return json_encode($result, true);
                exit;
            }
        }
        $result = array('status' => false);
        return json_encode($result, true);
        exit;
    }

    public function getallactivity() {
        $data = LocalguideActivity::find()->asarray()->all();
        return json_encode($data, true);
    } 
    
}