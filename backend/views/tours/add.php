<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Like;
use frontend\models\TravAdsVisitors;
$this->title = 'Add Tours';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;

//Your Database Connection include file here.
//This Entire PHP part can be placed in a seperate action file
if(isset($_POST['submit']))
{
	echo '<pre/>';print_r($_POST);print_r($_FILES);exit;
	//Upload Directory path.
	$uploaddir = 'uploads/';
	//FilePath with File name.
	$uploadfile = $uploaddir . basename($_FILES["file"]["name"]);
	//Check if uploaded file is CSV and not any other format.
	if(($_FILES["file"]["type"] == "text/csv")){
		//Move uploaded file to our Uploads folder.
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $uploadfile)) 
		{
			//Import uploaded file to Database. $collection is defined in the connection file or can be defined here too
			$command = "mongoimport --db joins --collection tours --file " . $uploadfile ." --type csv --headerline" ;
			shell_exec($command);
			$msg = '';
		}
		else
		{
			$msg = 'Please Try Later!';
		}
	}
	else
	{
		$msg = 'Please Upload a CSV file Only!';
	}
}

?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Add Tours</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Tour List</h3>
					</div>
					<div class="box-body">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Upload CSV file</h3>
							</div>
							<form role="form" enctype='multipart/form-data' method='post' href="javascript:void(0)" name="tours_add">
							  <div class="box-body">
								<div class="form-group">
									<label for="exampleInputFile">Select File</label>
									<?php if(isset($msg) && !empty($msg)){ ?>
									<label for="exampleInputFile"><?=$msg;?></label>
									<?php } ?>
									<input type="file" id="exampleInputFile">
								</div>
							  </div>
							  <div class="box-footer">
								<button type="submit" class="btn btn-primary">Upload</button>
							  </div>
							</form>
						  </div>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>