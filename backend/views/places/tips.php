<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$this->title = 'Place Tips';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Place Tips</h1>
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Place Tips List</h3>
					</div>
					<div class="box-body">
						<table id="placestipslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Place</th>
								  <th>Tip</th>
								  <th>Tip By</th>
								  <th>Tip Date</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($tips as $post)
								{
									$postid = $post['_id'];
								?>
									<tr>
										<td><?= $post['currentlocation'];?></td>
										<td><?= $post['post_text'];?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $post['post_user_id'];?>"><?= $post['user']['fullname'];?></a></td>
										<td><?= date('d-M-Y',$post['post_created_date']);?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=site/travpost&postid=<?= $postid;?>">View</a> / <a href="javascript:void(0)" id="<?=$postid;?>" onclick="remove('<?= $postid;?>')">Delete</a></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this post?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=page/removepost', 
			type: 'POST',
			data: 'post_id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#placestipslist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>