<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
$this->title = 'Travstore Image';
?>
<div class="content-wrapper">
    <section class="content-header">
		<h1>Travstore Image</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Travstore Image</h3>
					</div>
					<div class="box-body">
						<table id="collectionimglist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Image</th>
								  <th>Updated Date</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><img height="150px" src="<?= $front_url;?>/uploads/defaultimage/<?= $defaultimage['image'];?>"></td>
									<td><?= date('d-M-Y',$defaultimage['updated_date']);?></td>
									<td><a href="javascript:void(0)">Update</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>