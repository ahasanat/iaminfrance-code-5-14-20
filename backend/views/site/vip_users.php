<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Vip Member Listing';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Vip Members</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Vip Member List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="vip_users" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
				  <th>Posts</th>
				  <th>Comments</th>
				  <th>Credit History</th>
				  <th>Password</th>
				  <th>Payment History</th>
				  
                </tr>
                </thead>
                <tbody>
    <?php foreach($vipusers as $userdata){ 
	$id = $userdata['_id'];
	?>
            <tr>
                <td><?= $userdata['fname'];?></td>
                <td><?= $userdata['lname'];?></td>
				<td><a href="<?php echo Url::to(['post/view-user-posts', 'user_id' => "$id"]);?>">View</a></td>
				<td><a href="<?php echo Url::to(['post/view-user-comments', 'user_id' => "$id"]);?>">View</a></td>
				<td><a href="<?php echo Url::to(['userdata/credit-history', 'user_id' => "$id"]);?>">Click Here</a></td>
				<td id="<?=$userdata['_id']?>"><a href="javascript:void(0)" onclick="reset_password('<?=$userdata['_id']?>')">Reset</a></td>
                <td><a href="<?php echo Url::to(['userdata/payment-history', 'user_id' => "$id"]);?>">Click Here</a></td>
                </tr>

            <?php }?>
                
                </tbody>
              </table>
			  <script>
				function reset_password(id)
				{
					var r = confirm("Are you sure to reset password for this user?");
					if (r == false) 
					{
						return false;
					}
					else 
					{
						$.ajax({
							url: '?r=userdata/reset-password', 
							type: 'POST',
							data: 'id=' + id,
							success: function (data) 
							{
								alert("Password Successfully Changed..!!");
							}
						});
					}
				}
			  </script>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
