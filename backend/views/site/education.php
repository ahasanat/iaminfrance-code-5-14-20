<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Education';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper addbuscat-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Education</h1>
     <?php  $session =
                    Yii::$app->session;
           echo  $email =
                    $session->get('username'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add Education</h3>
            </div>
			<div class="box-body">
				<form id="frm" class="topform">
					<div class="frow">
						<label>Please add education </label>&nbsp;
						<input type="text" name="name" id="name" required/><span class="name_notice" style="display: none"></span><br/>
					</div>
					<div class="frow">
						<input type="button" name="add" value="add" onclick="addbuscat()" class="btn btn-primary"/>  
						<input type="reset" name="clear" value="clear"  class="btn btn-primary"/>  
					</div>
				</form>
            </div>
            <script>
                function addbuscat(){
                    var name = $('#name').val();
                    if(name == '')
                    {
                        $('.name_notice').html('Please enter education');
                        $('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
                        $("#name").focus();
                        return false;
                    }
                    else
                    {
                        $.ajax({
                            url: '?r=site/education', 
                            type: 'POST',
                            data: 'name=' + name,
                            success: function (data) 
                            {
                                if(data == 'insert')
                                {
                                    $("#frm")[0].reset();
                                    $("#education").load(window.location + " #education");
                                }
                                else
                                {
                                    $('.name_notice').html('This education already exist');
                                    $('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
                                }
                            }
                        });
                    }
                }
            </script>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="education" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
            <?php foreach($bus_cats as $bus_cat){ ?>
            <tr>
                <td><?= $bus_cat['name'];?></td>
                <td><a id="<?= $bus_cat['_id'];?>" onclick="removeeducation('<?= $bus_cat['_id'];?>')">Delete</a></td>
            </tr>
            <?php }?>
                
                </tbody>
               
              </table>
            </div>
			<script>
			function removeeducation(id){
					var r = confirm("Are you sure to delete this education?");
					if (r == false) {
						return false;
					}
					else 
					{
						$.ajax({
								url: '?r=site/removeeducation', 
								type: 'POST',
								data: 'id=' + id,
								success: function (data) {
									$("#"+id).parents('tr').remove();	
								}
							});
					}
				}
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
