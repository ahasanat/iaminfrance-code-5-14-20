<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Like;
use frontend\models\TravAdsVisitors;
use backend\models\Googlekey;
$this->title = 'Google Keys';
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Google Keys</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
		            <div class="box-body no-padding">
		              <table class="table table-condensed">
		              	<thead>	
							<tr>
								<th></th>
								<th>Key</th>
								<th>Status</th>
								<th>Expired On</th>
							</tr>
		              	</thead>
		                <tbody>
		                <?php
		                $Googlekey = Googlekey::find()->asarray()->all();
		                if(!empty($Googlekey)) {
		                	foreach ($Googlekey as $S_Googlekey) {
		                		$id = (string)$S_Googlekey['_id'];
		                		$key = $S_Googlekey['key'];
		                		$status = $S_Googlekey['status'];
		                		$expired_on = isset($S_Googlekey['expired_on']) ? $S_Googlekey['expired_on'] : '';
		                		$rand = rand(9999, 9999999);
		                		$uniqid = $rand.uniqid();
		                		$cls = '';
		                		if($status == 'active') {
		                			$cls = 'checked';
		                		}

		                		if($status == 'expired') {
		                			$cls = 'disabled';
		                		}

		                		?>
				                <tr>
				                	<?php if($status == 'expired') { ?>
				                  	<td>
				                  			<input type="radio" <?=$cls?> id="<?=$uniqid?>">
					                        <label for="<?=$uniqid?>"></label>
					               	</td>
				                  	<td><?=$key?></td>
				                  	<?php } else { ?>
				                  	<td>
				                  		<a href="javascript:void(0)" onclick="setkey(this)" data-id="<?=$id?>">
					                  		<input type="radio" <?=$cls?> id="<?=$uniqid?>">
					                        <label for="<?=$uniqid?>"></label>
					                    </a>
				                  	</td>
				                  	<td>
				                  		<a href="javascript:void(0)" onclick="setkey(this)" data-id="<?=$id?>"><?=$key?></a>
				                  	</td>
				                  	<?php } ?>
				                  	<td><?=$status?></td>
				                  	<td><?=$expired_on?></td>
				                </tr>
		                		<?php
		                	}
		                }
		                ?>
		              </tbody></table>
		            </div>
		            <!-- /.box-body -->
		          </div>
			</div>
		</div>
    </section>
</div>
<script type="text/javascript">
	function setkey(obj) {
		var $this = $(obj);
		var id = $this.attr('data-id');
		if(id) {
			$.ajax({
	            type: 'POST',
	            url: "?r=google/setkey", 
	            data: {id},
	            success: function (data) {
	            	window.location.href="";
	            }
	        });
	    }
	}
</script>