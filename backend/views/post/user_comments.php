<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'Post Comment Listing';
$user = LoginForm::find()->where(['_id'=> $_GET['user_id']])->one();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Comments</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?=$user['fullname']?>'s Comment List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="usercommentlist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>comment</th>
                  <th>Delete</th>
                  
                </tr>
                </thead>
                <tbody>
    <?php foreach($commentlists as $commentlist){ ?>
            <tr>
                <td><?= $commentlist['comment'];?></td>
                <td id="<?= $commentlist['_id'];?>"><a onclick="delete_comment('<?= $commentlist['_id'];?>')" style="cursor: pointer;">Delete</a></td>
               
			</tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
			<script>
				function delete_comment(id){
					var r = confirm("Are you sure to delete this comment?");
					if (r == false) {
						return false;
					}
					else 
					{
						$.ajax({
							url: '?r=post/deletecomment', 
							type: 'POST',
							data: 'id=' + id,
							success: function (data) {
								
								 if(data == 1){
									 var row = $("#"+id).parents('tr');
										$('#usercommentlist').dataTable().fnDeleteRow(row); 
								 }
								 
								 
							}
						});
					}
				}
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
