$(document).ready(function() {
	justifiedGalleryinitialize();
	lightGalleryinitialize();
});

$(document).ready(function(){ 
	$w = $(window).width();
	if ( $w > 739) {      
	$(".places-tabs .sub-tabs li a").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	$(".tabs.icon-menu.tabsnew li a").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	$(".mbl-tabnav").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	$(".clicable.viewall-link").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	} else {
	$(".places-tabs .sub-tabs li a").click(function(){
	   $("body").addClass("remove_scroller");
	}); 
	$(".clicable.viewall-link").click(function(){
	   $("body").addClass("remove_scroller");
	});         
	$(".tabs.icon-menu.tabsnew li a").click(function(){
	   $("body").addClass("remove_scroller");
	}); 
	$(".mbl-tabnav").click(function(){
	   $("body").removeClass("remove_scroller");
	});
	}

	$(".header-icon-tabs .tabsnew .tab a").click(function(){
		$(".bottom_tabs").hide();
	});

	$(".places-tabs .tab a").click(function(){
		$(".top_tabs").hide();
	});



	// footer work for places home page only
	$('.footer-section').css('left', '0');
	$w = $(window).width();
	if($w <= 768) {
		$('.main-footer').css({
		   'width': '100%',
		   'left': '0'
		});
	} else {
		var $_I = $('.places-content.places-all').width();
		var $__I = $('.places-content.places-all').find('.container').width();

		var $half = parseInt($_I) - parseInt($__I);
		$half = parseInt($half) / 2;

		$('.main-footer').css({
		   'width': $_I+'px',
		   'left': '-'+$half+'px'
		});
	}
});

$(window).resize(function() {
	// footer work for places home page only
	if($('#places-all').hasClass('active')) {
		$('.footer-section').css('left', '0');
		$w = $(window).width();
		if($w <= 768) {
		   $('.main-footer').css({
		      'width': '100%',
		      'left': '0'
		   });
		} else {
		   var $_I = $('.places-content.places-all').width();
		   var $__I = $('.places-content.places-all').find('.container').width();

		   var $half = parseInt($_I) - parseInt($__I);
		   $half = parseInt($half) / 2;

		   $('.main-footer').css({
		      'width': $_I+'px',
		      'left': '-'+$half+'px'
		   });
		}
	}
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');
	$('.places-content').removeClass().addClass('places-content '+$href);
	$this = $(this);
});

$(document).on('click', '.upload-gallery-blog', function (obj) {
    if($(this).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(this).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        $.ajax({ 
            url: '?r=blog/createblogui',  
            success: function(data) {
                photoUpload = [];
                customArray = [];
                customArrayTemp = [];
                $('#upload-gallery-popup').html(data);    
                setTimeout(function() { 
                    $('#upload-gallery-popup').modal('open');
                    $('.chips').material_chip({
                        placeholder: 'Enter photo categories.'
                    });  
                }, 400);
            }
        });
    }
});

$(document).on('click', '.edit-gallery-blog', function (e) {
    var $editid = $(this).attr('data-editid');
    $.ajax({
        type: 'POST', 
        data:{$editid}, 
        url: '?r=blog/editblogui',  
        success: function(data) {
            photoUpload = [];
            customArray = [];
            customArrayTemp = [];
            $('#edit-gallery-popup').html(data);    
            setTimeout(function() { 
                $('#edit-gallery-popup').modal('open');
                $('#edit-gallery-popup').css('z-index', 2000);
                $('#compose_discard_modal').css('z-index', 2050);
            }, 400);
        }
    });
});


$(document).on('mouseover', '.collection-item.avatar', function() {
    $(this).find('.zmdi.zmdi-delete').show();
    $(this).find('.zmdi.zmdi-edit').show();
});

$(document).on('mouseout', '.collection-item.avatar', function() {
    $(this).find('.zmdi.zmdi-delete').hide();
    $(this).find('.zmdi.zmdi-edit').hide();
});

/* location search key press event handler */
$(document).on("keypress", "#blog_cmt_txtarea", function(e) {
    if(e.which == 13) {
        $this = $(this);
        $comment = $this.val();
        var formdata;
        formdata = new FormData();
        formdata.append('comment', $comment);
        $.ajax({ 
            type: 'POST', 
            url: "?r=blog/docomment", 
            data: formdata,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    $this.val('');
                    $comment = result.comment;
                    $count = result.count;
                    $('.comment-list').find('ul').append($comment);
                    $('.comment-count').find('h4').html($count+' Comments');
                    Materialize.toast('Commented successfully', 2000, 'green');
                }
            }
        });
    }
});

$(document.body).on("change", ".custom-upload-blog", function () {
    var getid = $(this).attr('id');
    var cls = $(this).data("class");
    if(cls==undefined || cls==null) return false;
    var newcls = cls;
    changePhotoFileInputblog(newcls, this, true);        
});

function changePhotoFileInputblog(cls, obj, t) {
    storedFiles = [];
    storedFilesExsting = [];
    
    var countFiles = $(obj)[0].files.length;
    var image_holder = $(cls);
    if ( window.FileReader && window.File && window.FileList && window.Blob ) {
        for (var i = 0; i < countFiles; i++) {
            var imgCls='';
            file = obj.files[i];
            filename = file.name;
            extn = filename.substr( (filename.lastIndexOf('.') +1) );
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "tif") {
                lastModified.push(file.lastModified);
                storedFiles.push(file);
                readImageblog( file,  image_holder );                   
                $(cls.replace(".img-row","").trim()).show();                              
                image_holder.show(); 
                $(".nice-scroll").getNiceScroll().resize();
                $(".post-photos").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)",autohidemode:"scroll"});
            }  else {
                Materialize.toast('Your photo has to be in file format of GIF, JPG, PNG or TIFF and less than 500k in size.', 2000, 'red');
                return false;
            }
        }

        if($(obj).hasClass('custom-upload-new')) {
            uploadstuff(obj);
        }

    } else {
        Materialize.toast('Your browser does not support file reader, please upgrade your browser.', 2000, 'red');
        return false;
    }

    initDropdown();
    mainFeedPostBtn();  
}

function readImageblog(file, prepandto) {
    ShowImage();
    var reader = new FileReader();
    reader.addEventListener("load", function (e) {
    var image  = new Image();
    image.addEventListener("load", function () {
        var imgCls = ''; 
        if(image.width>image.height){
            imgCls ="himg";
        } else if(image.width<image.height) {
            imgCls ="vimg";
        } else {
            imgCls ="himg";
        }

        $uploadImg = "<div class='img-box'> <img src='"+e.target.result+"' class='thumb-image "+imgCls+"'><a href='javascript:void(0)' class='removePhotoFileblog' onclick='removePhotoFileblog()' data-code='"+lastModified[newct]+"'> <i class='mdi mdi-close'></i> </a> </div>";

        $('.modal.open').find('.new_pic_add').find('.img-row').html($uploadImg);
    });
    image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;
    if (useBlob) {
      window.URL.revokeObjectURL(file);
    }
  });
  reader.readAsDataURL(file);  
}

function removePhotoFileblog() {
    storedFiles = [];
    storedFilesExsting = [];

    $uploadBox = "<div class='img-box'><div class='custom-file addimg-box add-photo ablum-add'> <span class='icont'>+</span> <br><span class=''>Upload photo</span> <div class='addimg-icon'> </div> <input class='upload custom-upload-blog remove-custom-upload' title='Choose a file to upload' required='' data-class='.post-photos .img-row' multiple='true' type='file'> </div></div>";
    $('.modal.open').find('.new_pic_add').find('.img-row').html($uploadBox);
}

function removepicblog_modal($id, obj)
{   
    $this = $(obj);
    var $src = $this.parents('.img-box').find('img').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=collections/removepic',  
            data: {$id, $src},
            success: function(data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $uploadBox = "<div class='img-box'><div class='custom-file addimg-box add-photo ablum-add'> <span class='icont'>+</span> <br><span class=''>Upload photo</span> <div class='addimg-icon'> </div> <input class='upload custom-upload-blog remove-custom-upload' title='Choose a file to upload' required='' data-class='.post-photos .img-row' multiple='true' type='file'> </div></div>";
                    $('.modal.open').find('.new_pic_add').find('.img-row').html($uploadBox);
                }
            }
        });
    }
}

function createblog() {
    applypostloader('SHOW');
    $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitle').val();
    $uploadpopupJIDSdescription = $('.upload-popupJIDS-description').val();
    
    // check validation
    if($uploadpopupJIDSphototitle == '') {
        Materialize.toast('Add title', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }
    
    if (storedFiles.length === 0) {
        Materialize.toast('Upload photos', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }

    var formdata;
    formdata = new FormData();
    for(var i=0, len=storedFiles.length; i<len; i++) {
        formdata.append('images', storedFiles[i]);
    }
    formdata.append('title', $uploadpopupJIDSphototitle);
    formdata.append('description', $uploadpopupJIDSdescription);
    
    $.ajax({ 
        type: 'POST', 
        url: "?r=blog/createblog", 
        data: formdata,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            //applypostloader('HIDE');
            Materialize.toast('Saved', 2000, 'green');
            window.location.href="";
        }
    });
}

function editblog(obj) {
    applypostloader('SHOW');
    $editid = $(obj).attr('data-editid');
    if($editid) {
        $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitleedit').val();
        $uploadpopupJIDSdescription = $('.upload-popupJIDS-descriptionedit').val();
        if($uploadpopupJIDSphototitle == '') {
            Materialize.toast('Add title', 2000, 'red');
            applypostloader('HIDE');
            return false;
        }
        
        if ($('.modal.open').find('.custom-file').length) {
            validate = false;
            Materialize.toast('Upload photo.', 2000, 'red');
            applypostloader('HIDE');
            return false;
        } else if (!$('.modal.open').find('.post-photos').find('.img-row').find('.img-box').length) {
            validate = false;
            Materialize.toast('Upload photo.', 2000, 'red');
            applypostloader('HIDE');
            return false;
        }

        var formdata;
        formdata = new FormData();
        for(var i=0, len=storedFiles.length; i<len; i++) {
            formdata.append('images', storedFiles[i]);
        }
        formdata.append('title', $uploadpopupJIDSphototitle);
        formdata.append('description', $uploadpopupJIDSdescription);
        formdata.append('id', $editid);
        $.ajax({
            type: 'POST', 
            url: "?r=blog/editblog", 
            data: formdata,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                //applypostloader('HIDE');
                Materialize.toast('Saved', 2000, 'green');
                window.location.href="";
            } 
        });     
    }
}

function deleteblog(obj, isPermission=false) {
    if(isPermission) {
        if($deleteid) {
            $.ajax({
                type: 'POST', 
                url: "?r=blog/deleteblog", 
                data: {$deleteid:obj},
                success: function (data) {
                    Materialize.toast('Deleted', 2000, 'green');
                    window.location.href="";
                }
            });     
        }
    } else {
        $deleteid = $(obj).attr('data-deleteid');
        var disText = $(".discard_md_modal .discard_modal_msg");
        var btnKeep = $(".discard_md_modal .modal_keep");
        var btnDiscard = $(".discard_md_modal .modal_discard");
        disText.html("Delete blog.");
        btnKeep.html("Keep");
        btnDiscard.html("Delete");
        btnDiscard.attr('onclick', 'deleteblog(\''+$deleteid+'\', true)');
        $(".discard_md_modal").modal("open");
    }
}

