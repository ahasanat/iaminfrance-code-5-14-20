var galleryfileeditnew = {};

var hideGallery = function(){
    $('.lg-backdrop').addClass('hidden');
    $('.lg-outer').addClass('hidden');
};
var showGallery = function(){
    $('.lg-backdrop').removeClass('hidden');
    $('.lg-outer').removeClass('hidden');
};
var preventGalleryShow = function(event, $galleryObj){ 
    event.preventDefault();
    event.stopPropagation();

    var $lgp = $('.lgt-gallery-photoGallery');
    if($lgp.length) {
        $lgp.lightGallery();
        $lgp.data('lightGallery').destroy();
    }

    var $lg = $('.lgt-gallery-photo');
    if($lg.length) {
        $lg.lightGallery();
        $lg.data('lightGallery').destroy();
    }

    var $lgnormal = $('.lgt-gallery-photonormal');
    if($lgnormal.length) {
        $lgnormal.lightGallery();
        $lgnormal.data('lightGallery').destroy();
    }

    hideGallery();
    $galleryObj.trigger('onAfterOpen.lg');
};

$(document).ready(function() {
    /*justifiedGalleryinitialize();
    lightGalleryinitialize();*/
}); 

$(document).on('click', '.lg-prev.lg-icon', function(event) {
    $iscurrent = $('.lg-current');
    if($iscurrent.length>0) {
        $isobject = $('.lg-current').find('.lg-object.lg-image');
        if($isobject.length >0) {
            $id = $isobject.attr('sizes');
            if($id) {
                onAfterSlideUW($id);
            }
        }
    }
});

$(document).on('click', '.lg-next.lg-icon', function(event) {
    $iscurrent = $('.lg-current');
    if($iscurrent.length>0) {
        $isobject = $('.lg-current').find('.lg-object.lg-image');
        if($isobject.length >0) {
            $id = $isobject.attr('sizes');
            if($id) {
                onAfterSlideUW($id);
            }
        }
    }
});
 
$(document).on('click', '.prevent-gallery', function(event) {
    $this = $(this);
    preventGalleryShow(event,$this);
});

$(document).on('click', '.allow-gallery', function(event) {
    showGallery(); 
});

$(document).on('click', '.allow-gallery .icons a.like i', function() {
    $(this).toggleClass('mdi-thumb-up-outline mdi-thumb-up');
});

$(document).on('click', '.removeedit-gallery-file-upload', function() {
    $this = $(this);
    $this.parents('.img-box').remove();

    if($('#edit-gallery-popup').find('.modal_content_container').length) {
        $id = $('#edit-gallery-popup').find('.modal_content_container').attr('data-editid');
        if($id) {
            $.ajax({
                url: "?r=userwall/getuploadgalleryimage", 
                type: 'POST',
                data: {id: $id},
                success: function (data) {
                    if(data) {
                        $('#edit-gallery-popup').find('.add-photo-block').find('.img-row').html(data);
                        $('#edit-gallery-file-upload').val('');
                        galleryfileeditnew = {};                        
                    }
                }
            });
        }
    }

});

$(document).on('click', '.userwall_tagged_users', function () {
    $('#userwall_tagged_users').find('.person_box').html('');
    $('#userwall_tagged_users').css('z-index', 2000);
    $('#userwall_tagged_users').modal('open');
});

$(document.body).on("change", ".edit-gallery-file-upload", function () {
    $this = $(this);
    if ( window.FileReader && window.File && window.FileList && window.Blob ) {
        file = document.getElementById('edit-gallery-file-upload').files[0];
        filename = file.name;
        extn = filename.substr( (filename.lastIndexOf('.') +1) );
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "tif") {
            ShowImage();
            var reader = new FileReader();
            reader.addEventListener("load", function (e) {
                var image  = new Image();
                image.addEventListener("load", function () {
                    var imgCls = ''; 
                    if(image.width>image.height){
                        imgCls ="himg";
                    } else if(image.width<image.height) {
                        imgCls ="vimg";
                    } else {
                        imgCls ="himg";
                    }

                    $photoHtml =  '<div class="img-box"><img src="'+e.target.result+'" class="thumb-image himg"><a href="javascript:void(0)" class="removeedit-gallery-file-upload"><i class="mdi mdi-close"></i></a></div>';
                     
                    $this.parents('.img-row').html($photoHtml);

                    galleryfileeditnew = file;
                });
                image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;
                if (useBlob) {
                    window.URL.revokeObjectURL(file);
                }
            });
            reader.readAsDataURL(file);                                            
        }  else {
            Materialize.toast('Your photo has to be in file format of GIF, JPG, PNG or TIFF and less than 500k in size.', 2000, 'red');
            return false;
        }
    }
});

$(document).on('click', '.img-row.layered .img-box', function (e) {
    $this = $(this);
    if($this.find('.custom-file').length) {
    } else {
        $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitle').val();
        $uploadpopupJIDSdescription = $('.upload-popupJIDS-description').val();
        $uploadpopupJIDSlocation = $('.upload-popupJIDS-location').val();
        $uploadpopupJIDStaggedconnections = userwall_tagged_users;
        $uploadpopupJIDSvisibleto = $('.upload-popupJIDS-visibleto').html().trim();
        
        var puttedData = {
            '$uploadpopupJIDSphototitle' : $uploadpopupJIDSphototitle,
            '$uploadpopupJIDSdescription' : $uploadpopupJIDSdescription,
            '$uploadpopupJIDSlocation' : $uploadpopupJIDSlocation,
            '$uploadpopupJIDStaggedconnections' : $uploadpopupJIDStaggedconnections,
            '$uploadpopupJIDSvisibleto' : $uploadpopupJIDSvisibleto
        }

        if($('.img-row.layered').find('.img-box.activelayered').length) {
            $oldindex = $('.img-row.layered').find('.img-box.activelayered').index();
            photoUpload[$oldindex] = puttedData;            
        } else {
            photoUpload[0] = puttedData;            
        }
        
        $('.img-row.layered').find('.img-box').removeClass('activelayered');
        $this.addClass('activelayered');
        $index = $('.img-row.layered .img-box').index($this);
        $('#layeredform')[0].reset();
        userwall_tagged_users = [];
        userwall_tagged_usersTemp = [];
        $('#upload-gallery-popup').find('.userwall_tagged_users').html('Add people to tagged connections');
        if(typeof photoUpload[$index] === 'undefined') {
        } else {
            $storeduploadpopupJIDSphototitle = photoUpload[$index].$uploadpopupJIDSphototitle;
            $storeduploadpopupJIDSdescription = photoUpload[$index].$uploadpopupJIDSdescription;
            $storeduploadpopupJIDSlocation = photoUpload[$index].$uploadpopupJIDSlocation;
            $storeduploadpopupJIDSvisibleto = photoUpload[$index].$uploadpopupJIDSvisibleto
            $storeduploadpopupJIDStaggedconnections = photoUpload[$index].$uploadpopupJIDStaggedconnections;

            $('.upload-popupJIDS-phototitle').val($storeduploadpopupJIDSphototitle);
            $('.upload-popupJIDS-description').val($storeduploadpopupJIDSdescription);
            $('.upload-popupJIDS-location').val($storeduploadpopupJIDSlocation);
            $('.upload-popupJIDS-visibleto').html($storeduploadpopupJIDSvisibleto);
            
            userwall_tagged_users = $storeduploadpopupJIDStaggedconnections;
            userwall_tagged_usersTemp = $storeduploadpopupJIDStaggedconnections;
    
            if(userwall_tagged_users.length) {
                $.ajax({
                    url: "?r=site/combileidwithname", 
                    type: 'POST',
                    data: {ids: userwall_tagged_users, for: 'tagged_users'},
                    success: function (data) {
                        var result = $.parseJSON(data);
                        if(result.status != undefined && result.status == true) {
                            var $html = result.html;
                            $('#upload-gallery-popup').find('.userwall_tagged_usersblock').html($html);
                        }
                    }
                });
            } else {
                $("#upload-gallery-popup").find(".userwall_tagged_usersblock").html("<span class='tagged_person_name userwall_tagged_users'>Add people to tagged connections</span>");
            }
        }
    }
});

$(document).on('click', '.upload-gallery', function (obj) {
    if($(this).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(this).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        $.ajax({ 
            url: '?r=gallery/fetchlayereduploadphotohtml',  
            success: function(data) {
                photoUpload = [];
                customArray = [];
                customArrayTemp = [];
                $('#upload-gallery-popup').html(data);    
                setTimeout(function() { 
                    $('#upload-gallery-popup').modal('open');
                    $('.chips').material_chip({
                        placeholder: 'Enter photo categories.'
                    });  
                }, 400);
            }
        });
    }
});

$(document).on('click', '.edit-gallery', function (e) {
    var $editid = $(this).attr('data-editid');
    var $type = $(this).attr('data-type');
    $.ajax({
        type: 'POST', 
        data:{$editid}, 
        url: '?r=gallery/fetcheditlayereduploadphotohtml',  
        success: function(data) {
            photoUpload = [];
            customArray = [];
            customArrayTemp = [];
            
            $('#edit-gallery-popup').html(data);    
            setTimeout(function() { 
                //lightGallerydestroy();

                $('#edit-gallery-popup').modal('open');
                $('.chips').material_chip({
                    placeholder: 'Enter photo categories.'
                });  
                
                $('#edit-gallery-popup').css('z-index', 2000);
                $('#compose_discard_modal').css('z-index', 2050);

                fetchgallerycategoriestaggeduser($editid);
            }, 400);
        }
    });
});

$('body').on( "keypress","#slidercommenttextarea",function (e) {
    if (e.which == 13) {
        var comment = $('#slidercommenttextarea').val();
        if(comment != '') {
            $iscurrent = $('.lg-current');
            if($iscurrent.length>0) {
                $isobject = $('.lg-current').find('.lg-object.lg-image');
                if($isobject.length >0) {
                    $ids = $isobject.attr('sizes');
                    $ids = $ids.split('|||');   
    
                    if($ids.length == 2) {
                        $id = $ids[0];
                        $type = $ids[1];
                        if($id) {
                            $imgSrc = $('.lg-current').find('img').attr('src');

                            var formdata; 
                            formdata = new FormData();
                            formdata.append("post_id", $id);
                            formdata.append("comment", comment);
                            formdata.append("type", $type);
                            formdata.append("imgsrc", $imgSrc);
                            $.ajax({ 
                                url: '?r=comment/slider-comment', 
                                type: 'POST',  
                                data:formdata, 
                                async: false,
                                processData: false,
                                contentType: false,
                                success: function (data) 
                                { 
                                    $('.comment-list').find('ul').prepend(data);
                                    $('#slidercommenttextarea').val('');
                                    getgallerycommentlikecount();
                                }
                            });
                        }
                    }
                }
            }
        }
    }
});

$('#userwall_tagged_users .chk_person_done_new').click(function (e) {
    if($(this).hasClass('focoutTRV03')) {
        return false;
    }

    userwall_tagged_users = userwall_tagged_usersTemp.slice();
    $('#userwall_tagged_users').modal('close');
    if(userwall_tagged_users.length) {
        $.ajax({
            url: "?r=site/combileidwithname", 
            type: 'POST',
            data: {ids: userwall_tagged_usersTemp, for: 'tagged_users'},
            success: function (data) {
                var result = $.parseJSON(data);
                if(result.status != undefined && result.status == true) {
                    var $html = result.html;

                    if($("#upload-gallery-popup").hasClass('open')) {
                        $("#upload-gallery-popup").find(".userwall_tagged_usersblock").html($html);
                    } else {
                        $("#edit-gallery-popup").find(".userwall_tagged_usersblock").html($html);
                    }
                }
            }
        });
    } else {
    	if($("#upload-gallery-popup").hasClass('open')) {
            $("#upload-gallery-popup").find(".userwall_tagged_usersblock").html("<span class='tagged_person_name userwall_tagged_users'>Add people to tagged connections</span>");
        } else {
            $("#edit-gallery-popup").find(".userwall_tagged_usersblock").html("<span class='tagged_person_name userwall_tagged_users'>Add people to tagged connections</span>");
        }
    }
});

$(document).on("keyup", "#userwall_tagged_users .search_box", function () {
    var $key = $("#userwall_tagged_users").find(".search_box").val().trim();
    $.ajax({ 
        url: "?r=site/add-user-for-tag-search", 
        type: 'POST', 
        data: {$key, userwall_tagged_usersTemp},
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = userwall_tagged_usersTemp.length;
                if(userCount>1) {
                    var label = userCount + ' users selected';
                } else {
                    var label = userCount + ' user selected';
                }
                $('#userwall_tagged_users').find('.selected_photo_text').html(label);
                if(content != '') {
                    $('#userwall_tagged_users').find('.person_box').html(content);
                } else {
                    getnolistfound('norecordfoundaddperson');
                }
            } else {
                getnolistfound('norecordfoundaddperson');
            }
        }
    });
});

$(document).on('click', '#userwall_tagged_users .person_detail_container', function () {
    $(this).find(".chk_person").click();
    var $thisvalue = $(this).find(".chk_person").val();    

    if($(this).find(".chk_person").prop('checked') == true){
        if($.inArray($thisvalue, userwall_tagged_usersTemp) !== -1) {
        } else {
            userwall_tagged_usersTemp.push($thisvalue);
        }
    } else {
        userwall_tagged_usersTemp = $.grep(userwall_tagged_usersTemp, function(value) {
          return value != $thisvalue;
        });
    }

    if(userwall_tagged_usersTemp.length >0) {
        $label = userwall_tagged_usersTemp.length + ' users selected.';
        $('#userwall_tagged_users').find('.chk_person_done_new').removeClass('focoutTRV03');
    } else {
        $label = '0 user selected.';            
        $('#userwall_tagged_users').find('.chk_person_done_new').addClass('focoutTRV03');
    }
    $('#userwall_tagged_users').find('.selected_photo_text').html($label);
});  

function fetchgallerycategoriestaggeduser($id) {
    if($id) {
        $.ajax({
            url: "?r=gallery/fetchgallerycategoriestaggeduser", 
            type: 'POST', 
            data: {$id},
            success: function (data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    var tagged_connections = result.tagged_connections;
                    userwall_tagged_users = tagged_connections;
                    userwall_tagged_usersTemp = tagged_connections;
                }
            }
        });            
    }
}

function userwall_tagged_users_fn() { 
    $.ajax({
        url: "?r=site/add-user-for-tag", 
        type: 'POST', 
        data: {ids: userwall_tagged_users},
        success: function (data) {
        	var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = userwall_tagged_users.length;
                if(userCount>1) {
                    var label = userCount + ' users selected';
                } else {
                    var label = userCount + ' user selected';
                }
                $('#userwall_tagged_users').find('.selected_photo_text').html(label);
                $('#userwall_tagged_users').find('.person_box').html(content);
            } else {
                $('#userwall_tagged_users').find('.selected_photo_text').html('0 user selected.');
                getnolistfound('norecordfoundtag');
            }
        }
    });
}

function galleryhidephoto(isPermission=false) {
    $iscurrent = $('.lg-current');
    if($iscurrent.length>0) {
        $isobject = $('.lg-current').find('.lg-object.lg-image');
        if($isobject.length >0) {
            $ids = $isobject.attr('sizes');
            $ids = $ids.split('|||');   
            if($ids.length == 2) {
                $id = $ids[0];
                if($id != undefined && $id != null && $id != '') {
                    $type = $ids[1];
                    $typeArray = ['UserPhotos', 'PostForm', 'PlaceDiscussion', 'Gallery'];
                    if($.inArray($type, $typeArray) !== -1) {
                        if(isPermission) {
                            $.ajax({   
                                url: '?r=gallery/galleryhidephoto',
                                type: 'POST',
                                data: {id: $id},
                                success: function(data)
                                { 
                                    var result = $.parseJSON(data);
                                    if(result.success != undefined) {
                                        var $lg = $('.lgt-gallery-photo');
                                        $lg.data('lightGallery').destroy();

                                        $HIJS = '.photos';
                                        if($($HIJS).length) { 
                                            $($HIJS).trigger('click');   
                                        }

                                        Materialize.toast('Hide', 2000, 'green');
                                    }
                                }       
                            });
                        } else {
                            var disText = $(".discard_md_modal .discard_modal_msg");
                            var btnKeep = $(".discard_md_modal .modal_keep");
                            var btnDiscard = $(".discard_md_modal .modal_discard");
                            disText.html("Hide this post.");
                            btnKeep.html("Keep");
                            btnDiscard.html("Hide");
                            btnDiscard.attr('onclick', 'galleryhidephoto(\''+$id+'\', true)');
                            $(".discard_md_modal").modal("open");
                            $(".discard_md_modal").css("z-index", "1100");
                        }
                    }
                }
            }
        }
    }
}
 
function temp() {
    applypostloader('SHOW');
    $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitle').val();
    $uploadpopupJIDSdescription = $('.upload-popupJIDS-description').val();
    $uploadpopupJIDSlocation = $('.upload-popupJIDS-location').val();
    $uploadpopupJIDSvisibleto = $('.upload-popupJIDS-visibleto').html().trim(); 
    $uploadpopupJIDStaggedconnections = userwall_tagged_users;
    $isPass = true;
    
    var puttedData = {
        '$uploadpopupJIDSphototitle' : $uploadpopupJIDSphototitle,
        '$uploadpopupJIDSdescription' : $uploadpopupJIDSdescription,
        '$uploadpopupJIDSlocation' : $uploadpopupJIDSlocation,
        '$uploadpopupJIDStaggedconnections' : $uploadpopupJIDStaggedconnections,
        '$uploadpopupJIDSvisibleto' : $uploadpopupJIDSvisibleto
    };

    if($('.img-row.layered').find('.img-box.activelayered').length) {
        $oldindex = $('.img-row.layered').find('.img-box.activelayered').index();
        photoUpload[$oldindex] = puttedData;            
    } else {
        photoUpload[0] = puttedData;            
    }

    var formdata;
    formdata = new FormData();
    for(var i=0, len=storedFiles.length; i<len; i++) {
        formdata.append('imageFile1[]', storedFiles[i]); 
        if(photoUpload[i] == undefined) {
            photoUpload[i] = [];
        }
    }

    for(var i=0, len=storedFilesExsting.length; i<len; i++) {
        $sadks = storedFilesExsting[i].name;
        formdata.append('imageFile2[]', $sadks); 
    }

    if (window.location.href.indexOf("places") > -1) {
        formdata.append("place", place);
        formdata.append("placetitle", placetitle);
    } else {
        formdata.append("place", "");
        formdata.append("placetitle", "");
    }

    var isPassed = 'NO';
    $.each(photoUpload, function(b, h) {
        if (h.$uploadpopupJIDSphototitle == null || h.$uploadpopupJIDSphototitle == undefined || h.$uploadpopupJIDSphototitle == '') {
        } else {
            $usrtitle = h.$uploadpopupJIDSphototitle;
            if ($usrtitle != "") {
                isPassed = 'YES';
            }
        }
    }); 

    if(isPassed == 'NO') {
        $('.upload-popupJIDS-phototitle').focus();
        Materialize.toast('Add title ds.', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }

    formdata.append('photoUpload', JSON.stringify(photoUpload)); 
    formdata.append('custom', customArray);
    if($isPass) {
        $.ajax({ 
            type: 'POST', 
            url: "?r=gallery/addgallery", 
            data: formdata,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                //applypostloader('HIDE');
                storedFiles = [];
                storedFilesExsting = [];
                userwall_tagged_users = [];
                userwall_tagged_usersTemp = [];
                customArrayTemp = [];
                customArray = [];
                photoUpload = [];
                $('#upload-gallery-popup').modal('close');
                var result = $.parseJSON(data);
                if(result.success != undefined) {
                    if(result.success) {
                        Materialize.toast('Saved', 2000, 'green');
                        if($(".photos").length) {   
                            $(".photos").trigger('click');
                        } else if (window.location.href.indexOf("photostream") > -1) {
                            window.location.href="";
                        } else {
                            gallleryContent(0, true);  
                        }
                        lightGallerydestroy();
                        lightGalleryinitializeforgallery();  
                    }
                }
            }
        });
    }
}

function tempedit(obj) {
    $editid = $(obj).attr('data-editid');
    if($editid) {
        applypostloader('SHOW');
        $uploadpopupJIDSphototitleedit = $('.upload-popupJIDS-phototitleedit').val();
        $uploadpopupJIDSdescriptionedit = $('.upload-popupJIDS-descriptionedit').val();
        $uploadpopupJIDSlocationedit = $('.upload-popupJIDS-locationedit').val();
        $uploadpopupJIDSvisibletoedit = $('.upload-popupJIDS-visibletoedit').html().trim();
        $uploadpopupJIDStaggedconnectionsedit = userwall_tagged_users;

        if ($uploadpopupJIDSphototitleedit == undefined || $uploadpopupJIDSphototitleedit == null || uploadpopupJIDSphototitleedit == "") {
            Materialize.toast('Enter title.', 2000, 'red');
            $('.upload-popupJIDS-phototitleedit').focus();
            applypostloader('HIDE');
            return false;
        }

        var formdata;
        formdata = new FormData();

        formdata.append('$uploadpopupJIDSphototitleedit', $uploadpopupJIDSphototitleedit); 
        formdata.append('$uploadpopupJIDSdescriptionedit', $uploadpopupJIDSdescriptionedit); 
        formdata.append('$uploadpopupJIDSlocationedit', $uploadpopupJIDSlocationedit); 
        formdata.append('$uploadpopupJIDStaggedconnectionsedit', $uploadpopupJIDStaggedconnectionsedit); 
        formdata.append('$uploadpopupJIDSvisibletoedit', $uploadpopupJIDSvisibletoedit); 
        formdata.append('image', galleryfileeditnew); 
        formdata.append('customids', customArray);
        formdata.append('id', $editid);
        $.ajax({
            type: 'POST', 
            url: "?r=gallery/editgallery", 
            data: formdata,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                //applypostloader('HIDE');
                storedFiles = [];
                storedFilesExsting = [];
                userwall_tagged_users = [];
                userwall_tagged_usersTemp = [];
                customArrayTemp = [];
                customArray = [];
                photoUpload = [];
                $('#edit-gallery-popup').modal('close');
                var result = $.parseJSON(data);
                if(result.success != undefined) {
                    if(result.success) {
                        Materialize.toast('Saved', 2000, 'green');
                        if($(".photos").length) {   
                            $(".photos").trigger('click');
                        } else if (window.location.href.indexOf("photostream") > -1) {
                            $('.lg-close.lg-icon').trigger('click');
                            window.location.href="";
                        } else {
                            gallleryContent(0, true);  
                        }
                        
                        onAfterSlideUW($editid+'|||Gallery');
                    }
                }

                getCurrentGallerySlideImg();
            }
        });     
    }
}

function getCurrentGallerySlideImg() {
    $iscurrent = $('.lg-current');
    if($iscurrent.length>0) {
        $isobject = $('.lg-current').find('.lg-object.lg-image');
        if($isobject.length >0) {
            $id = $isobject.attr('sizes');
            if($id) { 
                $isCurrent = $('.lg-current').length;
                if($isCurrent) {
                    $imgSrc = $('.lg-current').find('img').attr('src');
                    $.ajax({        
                        url: '?r=gallery/getcurrentgalleryslideimg',
                        type: 'POST',
                        data: {
                            id: $id,
                            imgsrc: $imgSrc
                        }, 
                        success: function(data) {
                            var result = $.parseJSON(data);
                            if(result.success != undefined && result.success == true) {
                                $imagesource = result.imgsrc;
                                $iscurrent.find('img').attr('src', $imagesource);
                            }
                        }       
                    });
                }
            }
        }
    }
}

function lightGallerydestroy() {
    var $lg = $('.lgt-gallery-photo');
    $lg.data('lightGallery').destroy();

    if($('.lg-close.lg-icon').length) {
        $('.lg-close.lg-icon').click();
    }

    $('#edit-gallery-popup').css('z-index', 1039);
    $('#compose_discard_modal').css('z-index', 1051);
}

function lightGalleryinitializeforgallery() {
    var $lgp = $('.lgt-gallery-photoGallery');
    
    $lgp.lightGallery({
        thumbnail:false,
        closable: false,
        mousewheel: false,
        loop: true
    });

    $lgp.show();
      
    $lgp.on('onAfterOpen.lg',function(event) {
        onAfterOpenUW();
    });

    $lgp.on('onCloseAfter.lg',function(event) {
        onCloseAfterUW();
    });
}

function lightGalleryinitialize() {
    var $lg = $('.lgt-gallery-photo');
    
    $lg.lightGallery({
        thumbnail:false,
        closable: false,
        mousewheel: false,
        loop: true
    });
    
    $lg.show();
    
    $lg.on('onAfterOpen.lg',function(event) {
        onAfterOpenUW();
    });

    $lg.on('onCloseAfter.lg',function(event) {
        onCloseAfterUW();
    });
}

function lightGalleryinitializenormal() {
    var $lgnormal = $('.lgt-gallery-photonormal');
    
    $lgnormal.lightGallery({
        thumbnail:true,
        closable: false,
        mousewheel: false,
        loop: true
    });
    
    $lgnormal.show();

    $lgnormal.on('onAfterOpen.lg',function(event) {
        $('html, body').css('overflowY', 'hidden'); 
        $('.lg-outer').css('overflowY', 'hidden'); 
    });

    $lgnormal.on('onCloseAfter.lg',function(event) {
        $('html, body').css('overflowY', 'auto'); 
        $('.lg-outer').css('overflowY', 'auto'); 
    });
}

function justifiedGalleryinitialize() {
    $('.lgt-gallery-justified').justifiedGallery({
        lastRow: 'nojustify',
        rowHeight: 220,
        maxRowHeight: 220,
        margins: 10, 
        sizeRangeSuffixes: {
            lt100: '_t',
            lt240: '_m',
            lt320: '_n',
            lt500: '',
            lt640: '_z',
            lt1024: '_b'
        }
    });
}

function initGalleryImageSlider() {
    $( ".images-container" ).each(function( index ) {
        
        $(this).removeClass (function (index, css) {
            return (css.match (/(^|\s)images-container\S+/g) || []).join(' ');
        });
        
        $(this).addClass('images-container images-container'+index);
    });
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
 
function onAfterSlideUW($id) {
    if($id) {
        var $imgSrc = $('.lg-current').find('img').attr('src');

        $.ajax({        
            url: '?r=gallery/getslidehtml',
            type: 'POST',
            data: {
                id: $id,
                imgsrc: $imgSrc
            }, 
            success: function(data) {
                $('#extraHtmlBlock').html(data);
                getgallerycommentlikecount();
            }       
        });
    }
}

function onCloseAfterUW() {
    $("body").css("overflow-y", "unset");
}

function onAfterOpenUW() {
    if($('.lg-outer').length) {
        $("body").css("overflow-y", "hidden");
        $block = "<div id='extraHtmlBlock' class='extraHtmlBlock gallery-container'> </div>";
        
        if($('.lg-outer').find('#extraHtmlBlock').length) {
            $('.lg-outer').find('#extraHtmlBlock').remove();
        }

        $('.lg-outer').append($block);
        getgallerycommentlikecount();

        $iscurrent = $('.lg-current');
        if($iscurrent.length>0) {
            $isobject = $('.lg-current').find('.lg-object.lg-image');
            if($isobject.length >0) {
                $id = $isobject.attr('sizes');
                if($id) {
                    onAfterSlideUW($id);
                }
            }
        }
    }
}

function getgallerycommentlikecount() {
    $iscurrent = $('.lg-current');
    if($iscurrent.length>0) {
        $isobject = $('.lg-current').find('.lg-object.lg-image');
        if($isobject.length >0) {
            var $ids = $isobject.attr('sizes'); 
            var $imgSrc = $('.lg-current').find('img').attr('src');
            $.ajax({         
                url: '?r=gallery/getgallerycommentlikecount',
                type: 'POST', 
                data: { 
                    ids: $ids, 
                    imgsrc: $imgSrc
                },
                success: function(data) { 
                    var result = $.parseJSON(data);
                    if(result.success != undefined && result.success == true) {
                        var likesCount = result.likesCount;
                        if(likesCount >0) {
                            likesCount = likesCount;
                            $('.likesCount').html(likesCount);
                        } else {
                            likesCount = '';
                            $('.likesCount').html('0');
                        }
                        var likehtml = result.likehtml;

                        var commentsCount = result.commentsCount;
                        
                        var tempid = result.tempid;
                        var likeIcon = result.likeIcon;
                        var commentIcon = result.commentIcon;

                        if(commentsCount >0) {
                            $('.commentsCount').html(commentsCount);
                        } else {
                            commentsCount = '';
                            $('.commentsCount').html('0');
                        }

                        $label = '';
                        $ids = $ids.split('|||'); 
                        if($ids.length == 2) { 
                            $label = $ids[1];
                        }

                        $bottomIcons = '<a class="lg-like lg-icon liveliketooltip" href="javascript: void(0)" data-label="'+$label+'" onclick="doLikeGallery(\''+tempid+'\', this)" data-title="'+likehtml+'"><i class="mdi '+likeIcon+'"></i></a><span class="lcount">'+likesCount+'</span><a class="lg-comment lg-icon" href="javascript: void(0)"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal mdi-23px"></i></a><a class="lg-comment lg-icon" href="javascript: void(0)" onclick="scrollToComment()"><i class="mdi '+commentIcon+'"></i></a><span class="lcount">'+commentsCount+'</span>';
                            
                        $('.photo-bottom-icons').html($bottomIcons);
                    }
                }       
            });
        }
    }
}
 

function doLikeGallery(pid, obj) {
    $type = $(obj).attr('data-label');
    $.ajax({ 
      url: '?r=like/like-post',  
      type: 'POST',
      data: 'post_id=' + pid,
      success: function (data) {
            var result = $.parseJSON(data);
            if(result.auth == 'checkuserauthclassnv') {
                checkuserauthclassnv();
            }  
            else if(result.auth == 'checkuserauthclassg') {
                checkuserauthclassg();
            } else {
                var title =  result.buddies;
                var count =  result.like_count; 

                if(count>0) {
                } else {
                    count = '';
                }
                
                $(obj).attr('data-title', title);
                $(obj).closest('.lcount').html(count);
                getgallerycommentlikecount(pid);
                getlikehtml(pid);

                if($type == 'UserPhotos') {
                    var like_status =  result.status;
                    var ls =  result.status;
                    var lsc = '';
                    if(ls =='1') {
                        lsc = 'Liked';
                    } else {
                        lsc = 'Like';
                    }

                    var cou = '';
                    if(count >0) {
                    cou = ' '+count+' ';
                    }

                    $(".ls_"+pid).html(lsc);
                    $(".likecount_"+pid).html(cou);
                    $('.liketitle_'+pid).attr("data-title", title);
                } else if($type == 'PostForm' || $type == 'PlaceDiscussion') {
                    var like_status =  result.status;
                    var cou = '';
                    if(count >0) {
                        cou = '<span class="tooltip_content lcount likecount_'+pid+'">'+count+'</span>';
                    }

                    if(like_status == '1') {
                        $(".liketitle_"+pid).addClass("active");
                    } else {
                        $(".liketitle_"+pid).removeClass("active"); 
                    }

                    $('.liketitle_'+pid).attr("data-title", title);
                    if($(".liketitle_"+pid).parents('.like-tooltip').find('.likecount_'+pid).length) {
                        $(".liketitle_"+pid).parents('.like-tooltip').find('.likecount_'+pid).remove(); 
                    }
                    $(".liketitle_"+pid).append(cou);
                } else if($type == 'Gallery') {
                    var cou = '<i class="zmdi zmdi-thumb-up"></i>';
                    var like_status =  result.status;
                    if(count >0) {
                        cou = '<span class="lcount likecount_'+pid+'">'+count+'</span>';
                    }
                    
                    if(like_status == '1') {
                        $(".liketitle_"+pid).addClass("active");
                    } else {
                        $(".liketitle_"+pid).removeClass("active"); 
                    }

                    $(".liketitle_"+pid).html(cou);
                    $('.liketitle_'+pid).attr("data-title", title); 
                }
            }
        }
    });
}

function scrollToComment(){
    $('#slidercommenttextarea').focus();
}

function getlikehtml(id){
    if(id){
        $.ajax({
            url: '?r=gallery/likehtml', 
            type: 'POST',
            data: {id},
            success: function (data) {
                $('.likehtml').html(data);
            }
        });
    }
}