$(document).ready(function() {
	justifiedGalleryinitialize();
	lightGalleryinitialize();
});

$(document).on('click', '.compose_newask', function () {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
	    $.ajax({
	        url: '?r=questions/composenewask',
	        success: function(data){
	            $('#compose_newask').html(data);
	            setTimeout(function() { 
	                initDropdown();
	                $('.tabs').tabs();
	                $('#compose_newask').modal('open'); 
	            }, 400);
	        }
	    });
	}
});

$(document).ready(function(){ 
	$w = $(window).width();
	if ( $w > 739) {      
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
	} else {
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").addClass("remove_scroller");
		});         
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		});
	}

	$(".header-icon-tabs .tabsnew .tab a").click(function(){
		$(".bottom_tabs").hide();
	});

	$(".places-tabs .tab a").click(function(){
		$(".top_tabs").hide();
	});

	

	// footer work for places home page only
	$('.footer-section').css('left', '0');
	$w = $(window).width();
	if($w <= 768) {
		$('.main-footer').css({
		   'width': '100%',
		   'left': '0'
		});
	} else {
		var $_I = $('.places-content.places-all').width();
		var $__I = $('.places-content.places-all').find('.container').width();

		var $half = parseInt($_I) - parseInt($__I);
		$half = parseInt($half) / 2;

		$('.main-footer').css({
		   'width': $_I+'px',
		   'left': '-'+$half+'px'
		});
	}

	aboutplace(3);
});

$(window).resize(function() {
	
	// footer work for places home page only
	if($('#places-all').hasClass('active')) {
		$('.footer-section').css('left', '0');
		$w = $(window).width();
		if($w <= 768) {
		   $('.main-footer').css({
		      'width': '100%',
		      'left': '0'
		   });
		} else {
		   var $_I = $('.places-content.places-all').width();
		   var $__I = $('.places-content.places-all').find('.container').width();

		   var $half = parseInt($_I) - parseInt($__I);
		   $half = parseInt($half) / 2;

		   $('.main-footer').css({
		      'width': $_I+'px',
		      'left': '-'+$half+'px'
		   });
		}
	}
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');

	$('.places-content').removeClass().addClass('places-content '+$href);

	
	$this = $(this);
});

function addAsk(obj) {
	if($(obj).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(obj).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		applypostloader('SHOW');
		
		var status = $("#user_status").val();
		if(status == 0) { applypostloader('HIDE'); return false; }

		var $select = $('#compose_newask');
		if($select.find('#textInput').val().trim().length <= 0) {
			Materialize.toast('Write something.', 2000, 'red');
			$select.find('#textInput').focus();
			applypostloader('HIDE');
			return false;
		}

		var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
		var title = $select.find("#title").val();
		chk = 'off';
		if($('#customchk:checkbox:checked').length > 0) {
			chk = 'on';
		}
		
		var status = $select.find('#textInput').val();
		if (status != "" && reg.test(status) == true) {
			 status = status.replace(reg, " ");
		}
		if (title != "" && reg.test(title) == true) {
			 title = title.replace(reg, " ");
		}
		var count = $select.find('hiddenCount').val();

		$(".post_loadin_img").css("display","inline-block");
		var formdata;
		formdata = new FormData($('form')[1]); 
		formdata.append("test", status);
		formdata.append("title", title);
		formdata.append("current_location", place);
		managePostButton('HIDE'); 
		$.ajax({ 
			url: '?r=questions/add-ask', 
			type: 'POST',
			data:formdata,
			async:false,        
			processData: false,
			contentType: false,
			success: function(data) {
				//applypostloader('HIDE');
				$('#compose_newask').modal('close');
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} else {
					Materialize.toast('Published.', 2000, 'green');
					newct = 0;
					lastModified = [];
	                storedFiles = [];
	                storedFilesExsting = [];
	                storedFiles.length = 0;
	                storedFilesExsting.length = 0;

	                if($('.nm-postlist').find('.joined-tb').length) {
	                	$('.nm-postlist').find('.joined-tb').parent('.post-holder').remove();
	                }

	                $(".nm-postlist").prepend(data);

					setTimeout(function() { 
			            initDropdown();
			            fixImageUI('newpost');
			        }, 400);
				}	
			}
		});
		return true;
	}
}

function edit_ask(pid) {
	if (pid != '') {
		applypostloader('SHOW');

		var formdata;
		var $select = $('#compose_newask');
		formdata = new FormData($('form')[1]);

		var $title = $select.find("#title").val();
		var $desc = $select.find('#edittextInput').val();

		if($.trim($desc).length <= 0) {
			Materialize.toast('Write something.', 2000, 'red');
			$select.find('#edittextInput').focus();
			applypostloader('HIDE');
			return false;
		}

		formdata.append("desc", $desc);
		formdata.append("pid", pid);
		formdata.append("title", $title);
		formdata.append("edit_current_location", place);
		$.ajax({
			type: 'POST',
			url: '?r=questions/edit-ask',
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data) {
				//applypostloader('HIDE');
				$select.modal('close');
				Materialize.toast('Published.', 2000, 'green');
				newct = 0;
				lastModified = [];
                storedFiles = [];
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
				$('#hide_'+pid).remove();
				$(".nm-postlist").prepend(data);
				setTimeout(function() { 
		            initDropdown();
		            fixImageUI('newpost');
		        }, 400);
			}
		});
	}
}

function deleteAsk(p_uid, pid, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=questions/delete-ask',
			type: 'POST',
			data: 'post_user_id=' + p_uid + "&pid=" + pid,
			success: function (data){
				$(".discard_md_modal").modal("close");
				if(data != '1'){
					var share_post_id = data;
					var share_counter = $("#shareid_"+share_post_id).html();
					if(share_counter !=''){
						var share_counter = share_counter.replace('(', '');
						var share_counter = share_counter.replace(')', '');
						var share_counter = parseInt(share_counter)-1;
						if(share_counter >= 1) {
							$("#shareid_"+share_post_id).html(' ('+share_counter+')');
						} else {
							$("#shareid_"+share_post_id).html('');
						}
					} else {
						$("#shareid_"+share_post_id).html(' (1)');
					}
				}	
				$('#hide_'+pid).remove();

				var $totalbox = $('.nm-postlist').find('.bborder.post-holder').length;
				if($totalbox <= 0) {
					$('.lt').html('(0)');	
					$('.nm-postlist').html('<div class="post-holder bshadow"><div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>Become a first to ask a question for this place</p> </div></div>');
				} else {
					$('.lt').html('('+$totalbox+')');	
				}
				Materialize.toast('Deleted.', 2000, 'green');
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete post?");
        btnKeep.html("Keep");
        btnDiscard.html("Delete");
        btnDiscard.attr('onclick', 'deleteAsk(\''+p_uid+'\', \''+pid+'\', true)');
        $(".discard_md_modal").modal("open");
	}
}