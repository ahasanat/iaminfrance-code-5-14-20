/************ CHAT FUNCTIONS ************/

/*$(window).resize(function() {		
	setTimeout(function() { 
		setChatHeight();
		setFloatChat();
		manageChatbox();
	}, 3000);
});*/	

$(document).ready(function() {

	/* manage chat box */  
	manageChatbox();
	/* end manage chat box */
	
	/* set chat window height */
	setChatHeight();		
	/* end set chat window height */

	/* set floating chat */
	setFloatChat();		
	/* end set floating chat */

	$(document).on("click", ".chat-online li", function() {
	  $('.chat-area').show();
	 });
	
	$(document).on("click", ".close-btn", function() {
	  $(this).parents('.chat-area').hide();
	 });
	/* set floating chat */
	$(".chat-button,.close-chat").click(function() {
		
		if (!($(this).hasClass('directcheckuserauthclass'))) {

			$(".float-chat").toggleClass("chat-open");
			
			var win_w=$(window).width();
			if(win_w<=1024){
				removeNiceScroll(".nice-scroll");
			}
			if(win_w<=767){
				if(!$(".float-chat").hasClass("chat-open")){
					//$('body').removeClass("nooverflow");
				}			
			}
			if(!$(".float-chat").hasClass("chat-open")){
				$(".chat-window").removeClass("shown");
			}
		}
	});
});

/* FUN set floating chat */
	/*function setFloatChat(){		
		var w_width=$(window).width();
		$(".float-chat").addClass("floating");		
	}*/
/* FUN end set floating chat */

/* FUN set chat window height */
	/*function setChatHeight(){
		var windowW = $(window).width();
		if (windowW <= 1024){
			//removeNiceScroll(".nice-scroll");
			var chatwindH = $('.chat-window ul.mainul').height();
			var backbtnH = $('.backChatList').height();
			var chattitleH = $('.chatwin-box .topbar').height();
			var chatTextareaH = $('.chat-add').height();
			var totaldiductH = backbtnH + chattitleH + chatTextareaH;

			$('.chat-conversation .chat-area').height(chatwindH - totaldiductH - 170);
		}
		else{			
			$('.chat-conversation .chat-area').height(200);
			$(".nice-scroll").getNiceScroll().resize();
		}
	}*/

	$.fn.isVisible = function() {
		// Current distance from the top of the page
		var windowScrollTopView = $(window).scrollTop();

		// Current distance from the top of the page, plus the height of the window
		var windowBottomView = windowScrollTopView + $(window).height();

		// Element distance from top
		var elemTop = $(this).offset().top;

		// Element distance from top, plus the height of the element
		var elemBottom = elemTop + $(this).height();

		return ((elemBottom <= windowBottomView) && (elemTop >= windowScrollTopView));
	}
/* FUN end set chat window height */

/* FUN manage chatbox */
	/*function manageChatbox(){
		var winw=$(window).width();				
		if(winw<=1024){			
			if(!$(".float-chat").hasClass("floating")){				
				$(".float-chat").find(".chat-tabs").addClass("shown");
				$(".float-chat").find(".chat-window").addClass("hidden");
			}
		}
		else{
			
			$(".float-chat").find(".chat-tabs").removeClass().addClass("chat-tabs");
			$(".float-chat").find(".chat-window").removeClass().addClass("chat-window");		
		}	
	}*/
/* FUN end manage chatbox */

/* FUN open-close chat window */
	function resetChatboxHolder() {
		$(".chat-window .mainul li").removeClass("active");
	}
	function openChatbox(obj) {		
		$(obj).addClass("active");

		var userid=$(obj).attr("id");
		var filterUserid = userid.replace("chat_","");
		
		/* AJAX call to replace messages part */
		var exists=false;
		$(".chat-window .mainul li.mainli").each(function() {
			var li_id=$(this).attr("id");
			li_id=li_id.replace("li-","");
			
			if(li_id==userid){
				exists=true;				
			}
		});

		if(exists) {
			resetChatboxHolder();
			$(".chat-window .mainul li#li-"+userid).addClass("active");
		} else {
			// generate new li
			resetChatboxHolder();			

			var imgsrc=$(obj).find(".img-holder").find("img").attr("src");
			var username=$(obj).find(".desc-holder").find(".uname").html();
				 
			$(".chat-window .mainul").append("<li class='mainli active' id='li-"+filterUserid+"'> <div class='chatwin-box bshadow'> <div class='topbar'> <div class='userinfo'> <img src='"+imgsrc+"'/> <h6>"+username+"</h6></div> <div class='actions'> <div class='chat-search'><input type='text' class='keywordMarkHighlighter' placeholder='search a keyword'><div class='btn-holder'> <a href='#' onclick='openChatSearch(this,\"close\")'><img src='"+$assetsPath+"/images/cross-icon.png'/></a></div> </div> <a href='javascript:void(0)' onclick='openChatSearch(this,\"open\")'><i class='zmdi zmdi-search'></i></a> <a href='javascript:void(0)' onclick='closeChatbox(this)'><i class='mdi mdi-close'></i></a> </div> </div> <div class='chat-scroll nice-scroll'> <div class='chat-conversation'> <div class='chat-area nice-scroll'><div class='chat-mainul images-container'><ul class='outer'></ul><span class='clearfix'><span></div> </div> </div><div class='chat-add'> <textarea placeholder='Write your message here' class='materialize-textarea' id='inputChatWall'></textarea> <div class='chat-action emotion-holder'><div class='emotion-box'><div class='nice-scroll emotions'><ul class='emotion-list'></ul></div></div><a href='javascript:void(0)' onclick='manageEmotionBox(this)' class='emotion-btn'><i class='mdi mdi-emoticon'></i></a><div class='custom-file'> <div class='title'> <a href='#'> <i class='mdi mdi-paperclip'></i> </a></div> <input name='upload' id='chatWallFileUpload' class='upload custom-upload' title='Choose a file to upload' required='' data-class='.main-content .post-photos .img-row' multiple='' type='file'> </div><a href='#giftlist-popup' class='popup-modal giftlink' onmouseover='$(this).find(\"img\").attr(\"src\", \""+$assetsPath+"/images/giftcard-icon-hover.png\");' onmouseout='$(this).find(\"img\").attr(\"src\", \""+$assetsPath+"/images/giftcard-icon.png\");' onclick='manageEmoStickersBox(\"popular\", \"chat\")'><img src='"+$assetsPath+"/images/giftcard-icon.png'></a><a href='javascript:void(0)' class='send-chat' onclick='messageSendFromChat();'> <i class='mdi mdi-telegram'></i> </a> </div> </div> </div> </div> </li>");

				chatLiClicked(obj);
		} 
		 
		$('.chat-window .mainul li.mainli').find('textarea#inputChatWall').attr('data-id', filterUserid);

		var isMobileChat=$(".float-chat").hasClass("floating");
		if(isMobileChat) {
			var winw=$(window).width();				
			if(winw<=1024){			
				$(".float-chat").find(".chat-tabs").addClass("hidden");
			}
			if($(".float-chat").find(".chat-window").hasClass("hidden")) {
				$(".float-chat").find(".chat-window").removeClass("hidden");
				$(".float-chat").find(".chat-window").addClass("shown");
			} else {
				$(".float-chat").find(".chat-window").addClass("shown");
			}
		} else {
			$(".float-chat").find(".chat-tabs").removeClass().addClass("chat-tabs");
			$(".float-chat").find(".chat-window").removeClass().addClass("chat-window");
		}
		//initNiceScroll(".nice-scroll");
		setChatHeight();
		setTimeout(function(){scrollChatBottom();},500);
				
	}
	function closeChatboxes(){
		var isMobileChat=$(".float-chat").hasClass("floating");
		if(isMobileChat){
			$(".float-chat").find(".chat-window").addClass("hidden");
			if($(".float-chat").find(".chat-tabs").hasClass("hidden")){
				$(".float-chat").find(".chat-tabs").removeClass("hidden");
				$(".float-chat").find(".chat-tabs").addClass("shown");
			}			
		}else{
			resetChatboxHolder();
		}
	}
	function closeChatbox(obj){
		var sparent=$(obj).parents(".mainli");		
		sparent.removeClass("active");
		var isMobileChat=$(".float-chat").hasClass("floating");
		if(isMobileChat){
			closeChatboxes();		
		}
	}
	function manageEmotionBox(obj,fromWhere){
		var selector=$(obj).parents(".emotion-holder");
		selector.find(".emotion-box").toggle(200);
		initNiceScroll(".nice-scroll");
		
		var $faces = [':)',':(',':D','8)',':o',';(','(sweat)',':*',':P','(blush)',':^)','|-)','|(','(inlove)',']:)','(talk)','|-(','(puke)','(doh)','x-(','(wasntme)','(party)',':S','(mm)','8-|',':x','(hi)','(call)','(devil)','(angel)','(envy)','(wait)','(hug)','(makeup)','(giggle)','(clap)',':?','(bow)','(rofl)','(whew)','(happy)','(smirk)','(nod)','(shake)','(punch)','(emo)','(y)','(n)','(handshake)','(skype)','<3','(u)','(e)','(f)','(rain)','(sun)','(time)','(music)','(movie)','(ph)','(coffee)','(pizza)','(cash)','(muscle)','(cake)','(beer)','(d)','(dance)','(ninja)','(*)','(mooning)','(finger)','(bandit)','(drunk)','(smoking)','(toivo)','(rock)','(headbang)','(bug)','(fubar)','(poolparty)','(swear)','(tmi)','(heidy)','(MySpace)','(malthe)','(tauri)','(priidu)'];
		$(".emotion-box ul.emotion-list").html("");
		$.each($faces, function(i, v){
			var faces = $.emoticons.replace(v);
            $(".emotion-box ul.emotion-list").append('<li><a href="javascript:void(0)" data-class="'+v+'" onclick="appendEmotion(\''+ v + '\',\''+ fromWhere + '\');">'+faces+'</a></li>');
		});
	}
	function appendEmotion(emovalue,fromWhere){		
		if(fromWhere=="messages") {
			var getText=$(".addnew-msg .write-msg").find("textarea").val();
			$(".addnew-msg .write-msg").find("textarea").val(getText+emovalue);
		} else {
			var getText=$(".chat-add").find("textarea").val();
			$(".chat-add").find("textarea").val(getText+emovalue);
		}
	}
	function getParameterByName(name) {
	    var url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
/* FUN end open-close chat window */
	
/* FUN open chat search */
	function openChatSearch(obj,doWhat){
		
		var sparent=$(obj).parents(".actions");
		if(doWhat=="open")
			sparent.find(".chat-search").fadeIn();
		else
			sparent.find(".chat-search").fadeOut();
	}
/* FUN end open chat search */

/* FUN new chat line added */
	function scrollChatBottom(){
		
		if($('.chat-window').find('.mainli.active').length) {
			var scrollHeight= $(".chat-window .mainli.active .chat-mainul").height();		
			
			setTimeout(function(){
				var windowW = $(window).width();
				if (windowW > 1024){
					if(!$(".chat-window .mainli.active .chat-area.nice-scroll").getNiceScroll().length){
						initNiceScroll(".nice-scroll");
					}
					$(".chat-window .mainli.active .chat-area.nice-scroll").getNiceScroll(0).doScrollTop(scrollHeight, -1); 
				}else{
					removeNiceScroll(".nice-scroll");
				}				
				setOpacity($('.chat-window .mainli.active .chat-area.nice-scroll'),1);				
			},500);
		}
	}
	function setOpacity(obj,opacity){
		$(obj).css("opacity",opacity).change();
	}
	function removeLoaderImg(obj){
		$(obj).css("background","none");
	}
	function setLoaderImg(obj){		
		$(obj).css("background","auto");
	}
	function appendEmotions(obj,comment){
		var textWithEmoticons = $.emoticons.replace(comment);
		var addHtml="<li class='chatli'> <ul class='submsg'> <li> <div class='msgholder'>"+textWithEmoticons+"</div> <div class='timestamp'>10:00 am</div> </li>  </ul> </li>";		
	}
	function scrollMessageBottom(){
		if($('.main-msgwindow .current-messages').find('.mainli.active').length) {
			var scrollHeight= $(".main-msgwindow .current-messages .mainli.active ul.outer").height();		
			setTimeout(function(){
				var windowW = $(window).width();
				if (windowW > 1024){
					if(!$(".main-msgwindow .current-messages .mainli.active .msgdetail-list.nice-scroll").getNiceScroll().length){
						initNiceScroll(".nice-scroll");
					}				
					$('.main-msgwindow .current-messages .mainli.active .msgdetail-list.nice-scroll').getNiceScroll(0).doScrollTop(scrollHeight, -1);
				}else{
					removeNiceScroll("nice-scroll");
				}
				setOpacity($('.main-msgwindow .current-messages .mainli.active .msgdetail-list.nice-scroll'),1);				
			},500);
		}
	}
/* FUN end new chat line added */

/************ END CHAT FUNCTIONS ************/