var geocoder;
var map;

/* Facebook Login */        
var signinWin;
var popupWidth=600;
var popupHeight=250;
var xPosition=($(window).width()-popupWidth)/2;
var yPosition=($(window).height()-popupHeight)/2;
$(document).on('click', '#FacebookBtn', function(obj) {				
   signinWin = window.open("?r=site/auth&authclient=facebook", "SignIn", "width="+popupWidth+",height="+popupHeight+",toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left="+xPosition+",top="+yPosition);
   signinWin.focus();
   return false;
}); 
/* Facebook Login */ 

$(document).ready(function() {
	stopdiv('empty');
	stoplists('empty');
	notescount('empty');
});

$(".colordot").click(function() {
	var color = '';
	color = $(this).attr("data-color");
	$(".color-palette > li").each(function(){			
		$(this).removeClass('active');
	});
	$(this).parents("li").addClass('active');
	$("#tripcolor").val(color);
});
$("#homelocation").on("click", function(){
	if($('#homelocation').attr('checked'))
	{
		$("#startfrom").val(place);
	}
	else
	{
		$("#startfrom").val('');
	}
});
/************ START Design Code **********/ 
	function resetAllSectionLayers() {
		if($(".main-content").hasClass("trip-page")) {
			$(".trip-mapview .side-section .section-layer").each(function(){
				if($(this).hasClass("front"))
					$(this).removeClass("front");
			});
		}
	}
	
	function resetAllSectionSubLayers() {
		if($(".main-content").hasClass("trip-page")){
			$(".trip-mapview .side-section .section-sublayer").each(function(){
				$(this).attr("class","section-sublayer");
			});
		}
	}

	function showLayer(obj,which) {
		var sparent = $(obj).parents(".trip-mapview").find(".side-section");
		BackTripRouteMap(obj);
		resetAllSectionLayers();
		resetAllSectionSubLayers();
		sparent.find(".section-layer#trip-bookmark").removeClass("showit");
		if(which=="alltrip"){
			sparent.find(".section-layer#trip-list").addClass("front");
			trips();
		}
		if(which=="viewtrip"){
			sparent.find(".section-layer#trip-view").addClass("front");
			var tripid = $(obj).attr("data-tripid");
			viewtrip(tripid);
		}
		if(which=="edittrip"){
			sparent.find(".section-layer#trip-edit").addClass("front");
			var tripid = $(obj).attr("data-tripid");
			edittrip(tripid);
		}
		if(which=="newtrip"){
			if($(obj).hasClass('checkuserauthclassnv')) {
				checkuserauthclassnv();
			} else if($(obj).hasClass('checkuserauthclassg')) {
				checkuserauthclassg();
			} else {
				sparent.find(".section-layer#trip-new").addClass("front");
				newtrip();
			}
		}
		var win_w=$(window).width();
		if(win_w<=568){
			if(which=="alltrip"){
				$(".trip-wrapper").removeClass("mobile-fullview");
			}else{
				$(".trip-wrapper").addClass("mobile-fullview");
			}
		}
		setTimeout(function(){setGeneralThings(); },300);
	}

	// to open side layer
	function showSubLayer(obj,which,mode) {
		var sparent = $(obj).parents(".side-section");
		resetAllSectionSubLayers();
		sparent.find(".section-sublayer#"+which).attr("class","section-sublayer "+mode);
		sparent.find(".section-sublayer#"+which).addClass("shown");
		if(which=="notes"){notes();}
		if(which=="bookmarks"){bookmarks();}
		setTimeout(function(){ initNiceScroll(".nice-scroll"); setGeneralThings(); },500);
	}

	function hideSubLayer(obj) {
		var fparent = $(obj).parents(".section-sublayer");
		if(fparent.hasClass("shown"))
			fparent.attr("class","section-sublayer");
	}

	function showBookmarkDetail(obj) {
		var sparent = $(obj).parents(".side-section");
		resetAllSectionSubLayers();
		sparent.find(".section-layer#trip-bookmark").addClass("showit");
		displaybookmark();
	}

	function hideBookmarkDetail(obj) {
		var sparent = $(obj).parents(".side-section");
		sparent.find(".section-layer#trip-bookmark").removeClass("showit");
	}

	function openDetailTripMap(obj) {
		var leftSection=$(obj).parents(".desc-box");
		leftSection.hide();
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").hide();
	}

	function closeDetailTripMap(obj){
		var leftSection=$(obj).parents(".trip-mapview").find(".desc-box");
		leftSection.show();
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").hide();
	}

	function openAccommodations(obj,cityName){
		resetAllSectionSubLayers();
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").show();
		$("#tripplace").val(cityName);
		tripAcco('hotels');
		var win_w=$(window).width();
		if(win_w<=568){			
			var leftSection=$(obj).parents(".desc-box");			
			leftSection.hide();
		}
	}

	function BackTripRouteMap(obj) {
		var mapView=$(obj).parents(".trip-mapview").find(".map-box");
		mapView.find(".map-filter").hide();
	}
/************ END Design Code **********/

$(document).ready(function() {
	trips();
	showtripmap('home',place);
	$('body').on('change', '#trip_name', function(){
		var trip_name = $("#trip_name").val();
		trip_name = trip_name.charAt(0).toUpperCase() + trip_name.slice(1);
		$("#trip_name").val(trip_name);
	});
	$('body').on('change', '#trip_summary', function(){
		var trip_summary = $("#trip_summary").val();
		trip_summary = trip_summary.charAt(0).toUpperCase() + trip_summary.slice(1);
		$("#trip_summary").val(trip_summary);
	});
	$('body').on('change', '#notetitle', function(){
		var notetitle = $("#notetitle").val();
		notetitle = notetitle.charAt(0).toUpperCase() + notetitle.slice(1);
		$("#notetitle").val(notetitle);
	});
	$('body').on('change', '#notetext', function(){
		var notetext = $("#notetext").val();
		notetext = notetext.charAt(0).toUpperCase() + notetext.slice(1);
		$("#notetext").val(notetext);
	});

	initialize();
});

$(".colordot").click(function() {
	var color = '';
	color = $(this).attr("data-color");
	$(".color-palette > li").each(function(){			
		$(this).removeClass('active');
	});
	$(this).parents("li").addClass('active');
	$("#tripcolor").val(color);
});

function initialize() {
	var center = new google.maps.LatLng($getstartpoints);
	if($('#tripmap').length) {
		map = new google.maps.Map(document.getElementById('tripmap'), {
			center: center,
			zoom: 10,
			height: 450,
			width: 600,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		var bounds = new google.maps.LatLngBounds();
		var start = [$start]
		var end = [$end]
		for (var i=0; i < end.length; i++) {
			var startCoords = start[i].split(",");
			var startPt = new google.maps.LatLng(startCoords[0],startCoords[1]);
			var endCoords = end[i].split(",");
			var endPt = new google.maps.LatLng(endCoords[0],endCoords[1]);
			calcRoute(startPt, endPt);
			bounds.extend(startPt);
			bounds.extend(endPt);
		}
		map.fitBounds(bounds);
	}
}

function removeDivs() {
	$("#trip-new").html('');
	$("#trip-edit").html('');
	$("#trip-view").html('');
	$("#trip-list").html('');
}

function trips() {
	removeDivs();
	$.ajax({
		type: 'POST',
		url: '?r=trip/mytrips',
		data: 'baseUrl='+baseUrl,
		success: function(data) {
			$("#tripid").val('empty');
			$("#triptype").val('all');
			$("#trip-list").html(data);
			initNiceScroll(".nice-scroll");
			showtripmap('home',place); 
			setTimeout(function(){
				setGeneralThings();
				initDropdown();
			}, 4000);
		}
	});
}

function newtrip() {
	removeDivs();
	$.ajax({
		type: 'POST',
		url: '?r=trip/newtrip',
		data: 'baseUrl='+baseUrl+'&place='+place,
		success: function(data) {  
			$("#tripid").val('empty');
			$("#triptype").val('new');
			$("#trip-new").html(data);
			var yr=new Date().getFullYear();
			initNiceScroll(".nice-scroll");
			initDropdown();
		}
	});
}

function edittrip(tripid) {
	removeDivs();
	$.ajax({
		type: 'POST',
		url: '?r=trip/edittrip',
		data: 'baseUrl='+baseUrl+'&tripid='+tripid,
		success: function(data) {
			$("#trip-edit").html(data);
			$("#tripid").val(tripid);
			$("#triptype").val('new');
			initNiceScroll(".nice-scroll");
			showtripmap('view',tripid);
			setTimeout(function(){
				setGeneralThings(); 
				initDropdown();
			},300);
		}
	});
}

function viewtrip(tripid) {
	removeDivs();
	$.ajax({
		type: 'POST',
		url: '?r=trip/viewtrip',
		data: 'baseUrl='+baseUrl+'&tripid='+tripid,
		success: function(data) {
			$("#trip-view").html(data);
			$("#tripid").val(tripid);
			$("#triptype").val('view');
			initNiceScroll(".nice-scroll");
			showtripmap('view',tripid);
			showmobilemapdrops('view',tripid);
			initDropdown();
		}
	});
}

function notes(tripid) {
	$("#notes").html('');
	var tripid = $("#tripid").val();
	var triptype = $("#triptype").val();
	$.ajax({
		type: 'POST',
		url: '?r=trip/notes',
		data: 'baseUrl='+baseUrl+'&tripid='+tripid,
		success: function(data) {
			$("#notes").html(data);
			initNiceScroll(".nice-scroll");
			initDropdown();
		}
	});
}

function bookmarks() {
	$("#bookmarks").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/bookmarks',
		data: 'baseUrl='+baseUrl,
		success: function(data) {
			$("#bookmarks").html(data);
			initNiceScroll(".nice-scroll");
		}
	});
}

function displaybookmark() {
	$("#trip-bookmark").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/showbookmark',
		data: 'baseUrl='+baseUrl,
		success: function(data) {
			$("#trip-bookmark").html(data);
			initNiceScroll(".nice-scroll");
		}
	}); 
}

function showtripmap(which,place) {
	$("#trip-map").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/map',
		data: 'baseUrl='+baseUrl+'&place='+place+'&which='+which,
		success: function(data) {
			$("#trip-map").html(data);
		}
	});
}

function showmobilemapdrops(which,place) {
	$("#mobiledrops").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/mapdrops',
		data: 'baseUrl='+baseUrl+'&place='+place+'&which='+which,
		success: function(data) {
			$("#mobiledrops").html(data);
		}
	});
}

function showmobiletripmap(which,place) {
	$("#tripmap").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/mobilemap',
		data: 'baseUrl='+baseUrl+'&place='+place+'&which='+which,
		success: function(data) {
			$("#tripmap").html(data);
		}
	});
}

function tripAcco(which) {
	var tripplace = $("#tripplace").val();
	$("#trip-map").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/mapacco',
		data: 'baseUrl='+baseUrl+'&place='+tripplace+'&which='+which, 
		success: function(data) {
			$("#trip-map").html(data);
		}
	});
}

function stopdiv(tripid) {
	$("#stopdiv").html('');
	$.ajax({ 
		type: 'POST',   
		url: '?r=trip/stopdiv',
		data: 'baseUrl='+baseUrl+'&tripid='+tripid,
		success: function(data) {
			$("#stopdiv").html(data);
		}
	});
}

function stoplists(tripid) {
	$("#trip_stops").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/stoplists',
		data: 'baseUrl='+baseUrl+'&tripid='+tripid,
		success: function(data) {
			$("#trip_stops").html(data);
		}
	});
}

function notescount(tripid) {
	$("#notescount").html('');
	$.ajax({
		type: 'POST',
		url: '?r=trip/notescount',
		data: 'baseUrl='+baseUrl+'&tripid='+tripid,
		success: function(data) {
			$("#notescount").html(data);
		}
	});
}

function deletetripplace(place,tripid, isPermission=false) {
	$isLastStop = $("ul.tripstops-list").find("li").length;
	if($isLastStop == 1) {
		Materialize.toast('Add another stop then you can delete last stop.', 2000, 'red');
	} else {
		if(isPermission) {
			$.ajax({
				type: 'POST',
				url: '?r=trip/deltrplace',
				data: 'place='+place+'&tripid='+tripid,
				success: function(data)
				{
					$(".discard_md_modal").modal("close");
					stopdiv(tripid);
					stoplists(tripid);
					Materialize.toast('Stop deleted.', 2000, 'green');
				}
			});
		} else {
			$('.dropdown-button').dropdown("close");
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Delete stop.");
		    btnKeep.html("Keep");
		    btnDiscard.html("Delete");
		    btnDiscard.attr('onclick', 'deletetripplace(\''+place+'\', \''+tripid+'\', true)');
		    $(".discard_md_modal").modal("open");
		}		
	}
}

function deletenote(tripid,noteid) {
	$.ajax({
		type: 'POST',
		url: '?r=trip/delnote',
		data: 'noteid='+noteid+'&tripid='+tripid,
		success: function(data) {
			notescount(tripid);
			notes(tripid);
		}
	});
}

function deltrip(tripid, isPermission=false) {
	if(isPermission) {
		$.ajax({
			type: 'POST',
			url: '?r=trip/deltrp',
			data: 'tripid='+tripid,
			success: function(data) {
				$(".discard_md_modal").modal("close");
				if(data != 0) {
					$("#trip_"+tripid).remove('');
				} else {
					trips();
				}
				Materialize.toast('Trip deleted.', 2000, 'green');
			}
		});
	} else {
		$('.dropdown-button').dropdown("close");
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete trip");
	    btnKeep.html("Keep");
	    btnDiscard.html("Delete");
	    btnDiscard.attr('onclick', 'deltrip(\''+tripid+'\', this, true)');
	    $(".discard_md_modal").modal("open");
	} 
	
}

function deletetrip(e,tripid, isPermission=false) {
	if(isPermission) {
		$.ajax({
			type: 'POST',
			url: '?r=trip/deltrp',
			data: 'tripid='+tripid,
			success: function(data) {
				$(".discard_md_modal").modal("close");
				showLayer(e,'alltrip');
				Materialize.toast('Trip deleted.', 2000, 'green');
			}
		});
	} else {
		$('.dropdown-button').dropdown("close");
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete trip");
	    btnKeep.html("Keep");
	    btnDiscard.html("Delete");
	    btnDiscard.attr('onclick', 'deletetrip(\''+e+'\', \''+tripid+'\', true)');
	    $(".discard_md_modal").modal("open");
	} 
}

function saveEditNote(e) {
	var noteid = $(e).attr("data-noteid");
	var ntitle = $("#ntitle_"+noteid).val();
	var ntext = $("#ntext_"+noteid).val();
	$.ajax({
		url: '?r=trip/editnote',  
		type: 'POST',
		data:'noteid='+noteid+'&ntitle='+ntitle+'&ntext='+ntext,
		success: function(data) {
			close_detail(e);
			$(".ttitle_"+noteid).html(ntitle);
			$(".ttext_"+noteid).html(ntext);
		}
	});
}

function printTrip() {
	var which = $("#tripid").val();
	$("#tripprint").html('');
	$.ajax({
		url: '?r=trip/printtrip',  
		type: 'POST',
		data:'which='+which,
		success: function(data) {
			$("#tripprint").html(data);
			setTimeout(function(){
				var printContents = document.getElementById("tripprint").innerHTML;
				var originalContents = document.body.innerHTML;
				document.body.innerHTML = printContents;
				window.print();
				document.body.innerHTML = originalContents;
			},400);
		}
	});
}

function emailTrip() {
	var which = $("#tripid").val();
	$.ajax({
		url: '?r=trip/email',  
		type: 'POST',
		data:'which='+which,
		success: function(data) {
			var result = $.parseJSON(data);
			if(result['msg'] == 'success') {
				var msg = 'You\'ll get an email of all your trips';
				if(which != 'empty') {
					var msg = 'You\'ll get an email of the trip';
				}
				Materialize.toast(msg, 2000, 'green');
			} else {
				Materialize.toast('As you don\'t have trips to get an email so please create at least one trip', 2000, 'red');
			}
		}
	});
}

function addNewStop(type) {
	var trip_name = $("#trip_name").val();
	var addtripdate = $("#addtripdate").val();
	var startfrom = $("#startfrom").val();
	var nextstop = $("#nextstop").val();
	var trip_summary = $("#trip_summary").val();
	var mapline = $("input[name='radio1']:checked").val();
	var tripcolor = $("#tripcolor").val();
	var post_privacy = $("#post_privacy").val();
	var tripid = $("#tripid").val();
	var triptype = $("#triptype").val();
	var formdata;
	formdata = new FormData();
	if(triptype != 'view') {
		if(trip_name == '') {
			Materialize.toast('Enter trip name.', 2000, 'red');
			$("#trip_name").focus();
			return false;
		}
		if(addtripdate == '') {
			Materialize.toast('Trip start date.', 2000, 'red');
			$("#addtripdate").focus();
			return false;
		}
		if(startfrom == '') {
			Materialize.toast('Enter starting place.', 2000, 'red');
			$("#startfrom").focus();
			return false;
		}
		if(nextstop == '') {
			if(!($('.tripstops-list').find('li').length)) {
				Materialize.toast('Enter where you want to go.', 2000, 'red');
				$("#nextstop").focus();
				return false;
			}
		}
		if(type == 'stop') {
			var afterstop = $.trim($("#afterstop").val());
			if(afterstop == undefined || afterstop == null || afterstop == '') {
				$afterstop = '';
			}

			formdata.append("afterstop", afterstop);
		}
	}
	if(type == 'note') {
		var notetitle = $("#notetitle").val();
		var notetext = $("#notetext").val();
		if(notetitle == '') {
			Materialize.toast('Enter trip note title.', 2000, 'red');
			$("#notetitle").focus();
			return false;
		}
		if(notetext == '') {
			Materialize.toast('Enter trip note text.', 2000, 'red');
			$("#notetext").focus();
			return false;
		}
		formdata.append("notetitle", notetitle);
		formdata.append("notetext", notetext);
	}
	if(triptype != 'view') {
		formdata.append("trip_name", trip_name);
		formdata.append("addtripdate", addtripdate);
		formdata.append("startfrom", startfrom);
		formdata.append("nextstop", nextstop);
		formdata.append("trip_summary", trip_summary);
		formdata.append("mapline", mapline);
		formdata.append("tripcolor", tripcolor);
		formdata.append("privacy", post_privacy);
		formdata.append("type", type);
	}
	formdata.append("tripid", tripid);
	formdata.append("triptype", triptype);
	$.ajax({ 
		type: 'POST',
		url: '?r=trip/addtrip', 
		data: formdata,
		processData: false,
		contentType: false,
		success: function(data)
		{
 			$("#tripid").val(data);
			if(type == 'note')
			{
				Materialize.toast('Note added.', 2000, 'green');
				notescount(data);
				notes(data);
			} else if(type == 'stop') {
				$("#nextstop").val('');
				stopdiv(data);
				stoplists(data);
			} else {
				Materialize.toast('Trip created.', 2000, 'green');
				$('.tripmenu').find('ul').find('li:first').find('a').trigger('click');
			}
		}
	});
}

function changePlaceTripPlace() {
	var cityName = $("#mapdropplace").val();
	$("#tripplace").val(cityName);
	tripAcco('hotels');
}
 
/* Start Display Content For Sharing Functionality in Share Popup For The Post */
$(document.body).on('click', '.customsharepopup-modal', function(e) {
	var editpostid = $(this).data('sharepostid');
	
	$.ajax({
		type: 'POST',
		url: '?r=site/share-post-pre-set',
		data: 'pid='+editpostid,
		success: function(data){
			if(data) {
				if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} 
					else {

					$('#sharepostmodal').addClass("sharepost-popup-"+editpostid);
					$('#sharepostmodal').html(data);
					$('#sharepostmodal').modal('open');
					addUserForTag = [];

					e.preventDefault();
				}
				setTimeout(function(){
					setGeneralThings();
				},400);
			}
		}
	});        	
});
/* End Display Content For Sharing Functionality in Share Popup For The Post*/

/* Start Share Entity Function */
	function shareEntity(entity, isPermission=false) {
		if(isPermission) {
			applypostloader('SHOW');
			var sharewall = $("#sharewall").val();
			var frndid = $("#frndid").val();
			var desc = $("#share_desc").val();
			var spid = $("#spid").val();
			var post_privacy = $("#post_privacy").val();
			var share_current_location = $(".share_current_location").val();
			var share_setting = $('#share_setting').val();
			var comment_setting = $('#comment_setting').val();
			var posttags = $('#sharetag'+spid).val();
			if (desc === "" && frndid === "undefined")
			{
				$("#desc").focus(); 
				applypostloader('HIDE');
				return false;
			}
			else
			{
				
				$.ajax({
					type: 'POST',
					url: '?r=site/shareentity',
					data: "posttags=" + posttags + "&sharewall=" + sharewall + "&spid=" + spid + "&frndid=" + frndid + "&desc=" + desc + "&post_privacy=" + post_privacy + "&current_location=" + share_current_location + "&share_setting=" + share_setting + "&comment_setting=" + comment_setting,
					success: function (data) {
						//applypostloader('HIDE');
						$(".discard_md_modal").modal("close");
						window.location.href="";
						if(data) {
							$("#frndid").val('');
							$(".frnduname").val('');
						}
					}
				}).done(function(){
					setTimeout(function(){fixImageUI('newpost');},500);
				});
			}
		} else {
			$('.dropdown-button').dropdown("close");
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html('Share this '+entity+' on my wall.');
		    btnKeep.html("Keep");
		    btnDiscard.html("Share");
		    btnDiscard.attr('onclick', 'shareEntity(this, true)');
		    $(".discard_md_modal").modal("open");
		}
	}
/* Start Share Entity Function */