$(document).ready(function(){ 
  $w = $(window).width();
  if ( $w > 739) {      
    $(".places-tabs .sub-tabs li a").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
    $(".tabs.icon-menu.tabsnew li a").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
    $(".mbl-tabnav").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
    $(".clicable.viewall-link").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
  } else {
    $(".places-tabs .sub-tabs li a").click(function(){
       $("body").addClass("remove_scroller");
    }); 
    $(".clicable.viewall-link").click(function(){
       $("body").addClass("remove_scroller");
    });         
    $(".tabs.icon-menu.tabsnew li a").click(function(){
       $("body").addClass("remove_scroller");
    }); 
    $(".mbl-tabnav").click(function(){
       $("body").removeClass("remove_scroller");
    });
  }

  aboutplace(3);
  gethotellist();


  $(".home").click(function() {
    $('.cshfsiput').removeClass('cshfsi');
    $('#placedesc').html(''+place);
    rightplaceboxes();
  }); 

  $(document).on('click','body .allevents',function(){
    $('.cshfsiput').removeClass('cshfsi');
    $('#placedesc').html('Events of '+placefirst);
    rightplaceboxes();
    displayplace('all','events','empty');
  });

  $(".header-icon-tabs .tabsnew .tab a").click(function(){
    $(".bottom_tabs").hide();
  });

  $(".places-tabs .tab a").click(function(){
    $(".top_tabs").hide();
  });

  // footer work for places home page only
  $('.footer-section').css('left', '0');
  $w = $(window).width();
  if($w <= 768) {
    $('.main-footer').css({
       'width': '100%',
       'left': '0'
    });
  } else {
    var $_I = $('.places-content.places-all').width();
    var $__I = $('.places-content.places-all').find('.container').width();

    var $half = parseInt($_I) - parseInt($__I);
    $half = parseInt($half) / 2;

    $('.main-footer').css({
       'width': $_I+'px',
       'left': '-'+$half+'px' 
    });
  }

  $(".allhotels").click(function() { 
    $('.cshfsiput').removeClass('cshfsi');
    $('#placedesc').html('Hotels in '+placefirst);
    $('.cshfsiput').removeClass('cshfsi');
    displayplace('all','hotels','empty');
    if($("#distance-slider").length>0)
      initSlidernew("distance-slider",0,100,1,'km');
    if($("#price-slider").length>0)
      initSlidernew("price-slider",500,10000,2500,'$');
  });

  $(".allrest").click(function() {
    $('.cshfsiput').removeClass('cshfsi');
    $('#placedesc').html('Restaurants in '+placefirst);
    rightplaceboxes();
    displayplace('all','rest','empty');
  });
  
  $(".allthings").click(function() {
    $('.cshfsiput').removeClass('cshfsi');
    $('#placedesc').html('Attractions in '+placefirst);
    rightplaceboxes(); 
    displayplace('all','attr','empty');
  });

  var win_w=$(window).width();
  /* swich places main tabs */
  $("body").on("click", ".tabicon .icon-menu li", function(){
    resetPlaceTabs(".tablist .text-menu");        
    
    if($(this).find("a").attr("href")=="#places-lodge"){
      setPlacesSidebar("hide");
    } else {
      setPlacesSidebar("show");
    }
    $("body").getNiceScroll().resize();   
    setHideHeader(this,'places','none');
    manageNewPostBubble("hideit");
  });
  /* end swich places main tabs */

  $(document).on('click', '.newtabsnew .tabsnew a', function() {
      $href = $(this).attr('href');
    var initScroll = 190;
    var win_w = $(window).width();
    if(win_w < 767) {
      initScroll = 0;
    }
    $('html, body').animate({ scrollTop: initScroll }, 'slow');
    //tabsNewInit($href);
  });


  /* swich places sub tabs */
  $("body").on("click", ".tablist .text-menu li", function() {  

    if($(window).width()<=767) {
    
      if($('.home').find('a.active').length) {
        $('.header-section').show();
      } else {
        $('.header-section').hide();
      }
    } else {
      $('.header-section').show();
    }

    resetTabs(".tabicon .icon-menu");
    setPlacesSidebar("show");

    if($(this).find("a").html()=="Photos"){     
      if($('#placebox').find('.wcard').length) {
        $("#placebox").css("visibility","hidden");
      }
      setTimeout(function(){
        $("#placebox").css("visibility","visible");
        $(".gloader").hide();
        initGalleryImageSlider();
      },700);     
    }
    if($(this).find("a").html()=="Reviews"){        
      setTimeout(function(){fixImageUI();},400);
    }
    if($(this).find("a").html()=="Tip"){        
      setTimeout(function(){fixImageUI();},400);
    }

    if($(this).find("a").html()=="Reviews" || $(this).find("a").html()=="Tips" || $(this).find("a").html()=="Ask"){       
      manageNewPostBubble("showit");
    } else {
      manageNewPostBubble("hideit");
    }

    setHideHeader(this,'places','none');
  });
  /* end swich places sub tabs */ 
  /* fix images on places */
  if($(".main-content").hasClass("places-page")){ fixPlacesImageUI("all");}
  /* end fix images on places */
  /* set Wall tab default for mobile dropdown */
  if($(window).width()<=450){       
    $(".select2places").val("all").change();
  }
  /* set end Wall tab default for mobile dropdown */
  
  /* manage keepopen */
  if($(".page-wrapper").hasClass("place-wrapper")) {
    if( win_w<=450 ){
      if($(".search-holder").hasClass("keepopen")){
        $(".search-holder").removeClass("keepopen");
      }
    }else{
      if(!$(".search-holder").hasClass("keepopen")){
        $(".search-holder").addClass("keepopen");
      }
    }
  }
  /* end manage keepopen */

  $(document).on('click', '.place-wrapper .tab', function() {
    setplacetab();
    
  }); 

  /* things dropdown change */
  $(document).on('change','#thingsdropchange',function() {
    var stype = $("#thingsdropchange").val();
    displayplace('all','attr',stype);
  });

  setplacetab();

  // footer work for places home page only
  $('.footer-section').css('left', '0');
  $w = $(window).width();
  if($w <= 768) {
    $('.main-footer').css({
       'width': '100%',
       'left': '0'
    });
  } else {
    var $_I = $('.places-content.places-all').width();
    var $__I = $('.places-content.places-all').find('.container').width();

    var $half = parseInt($_I) - parseInt($__I);
    $half = parseInt($half) / 2;

    $('.main-footer').css({
       'width': $_I+'px',
       'left': '-'+$half+'px'
    });
  }

  $(".header-icon-tabs .tabsnew .tab a").click(function(){
    $(".bottom_tabs").hide();
  });
  
  $(".places-tabs .tab a").click(function(){
    $(".top_tabs").hide();
  });

  $('.new-tumb').carousel({
    dist:0,
    shift:0,
    padding:20,
    });

    $("#getplaceinfo").click(function() {
    var p = $('#place_loc').val();
    if(p != '')
    {
      window.location.href = "?r=places&p="+p;
    }
  });

    //tabsNewInit('#places-all');
    $('.profiletooltip_content').hide();
    
  $('.set_re_height .new_post_comment').on('keyup', function (ev) {
    $('.set_re_height .settings-icon').toggleClass('btn_enable', !!$(this).val().trim());
  });

  if($('.owl-carousel').length) {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
    margin: 10,
    loop: true,   
    dots: true,     
    nav: true,
    responsive: {
      0: {
      items: 2
      },
      600: {
      items: 3
      },   
      1000: {
      items: 4
      }
    }
    });
  }

  if($('#nightlypriceslideragain').length) {
    var slider = document.getElementById('nightlypriceslideragain');
    noUiSlider.create(slider, {
      start: [100, 10000],
      connect: true,
      step: 1,
      orientation: 'horizontal', // 'horizontal' or 'vertical'
      range: {
       'min': 100,
       'max': 10000
      },
      format: wNumb({
       decimals: 0
      }) 
    });
  }


});

$(window).resize(function() {
  setTimeout(function(){
    // footer work for places home page only
    if($('#places-all').hasClass('active')) {
      $('.footer-section').css('left', '0');
      $w = $(window).width();
      if($w <= 768) {
        $('.main-footer').css({
            'width': '100%',
            'left': '0'
        });
      } else {
        var $_I = $('.places-content.places-all').width();
        var $__I = $('.places-content.places-all').find('.container').width();

        var $half = parseInt($_I) - parseInt($__I);
        $half = parseInt($half) / 2;

        $('.main-footer').css({
            'width': $_I+'px',
            'left': '-'+$half+'px'
        });
      }
    }
  },800); 
});

$(document).on('click', '.tablist .tab a', function(e) {
    $href = $(this).attr('href');
      $href = $href.replace('#', '');
    
      $('.places-content').removeClass().addClass('places-content '+$href);
  });


$(document).on('click', '.tournumpeopledrp a', function() {
  $text = $(this).text();
  $content = $text+'<span class="mdi mdi-menu-down"></span>';
  $(this).parents('.person-info').find('.dropdown-button.btn').html($content);
});

$(window).resize(function() {
    $width = $(window).width();
    $totalImage = $('.pop-photos-sec').find('.allow-gallery').length;
    if($width <=600) {
      if($totalImage >=3) {
        $.ajax({
            type: 'POST',
            url: "?r=site/popluarphotoacrdscn",
            data: {w: $width},
            success: function (data) {
                $('.pop-photos-sec').find('#placebox').html(data);
                justifiedGalleryinitialize();
                lightGalleryinitialize();
            }
        });
      }
    } else {
      if($totalImage <=2) {
        $.ajax({
            type: 'POST',
            url: "?r=site/popluarphotoacrdscn",
            data: {w: $width},
            success: function (data) {
                $('.pop-photos-sec').find('#placebox').html(data);
                justifiedGalleryinitialize();
                lightGalleryinitialize();
            }
        });

      }
    }
});

$(window).resize(function() {
  // footer work for places home page only
  if($('#places-all').hasClass('active')) {
    $('.footer-section').css('left', '0');
    $w = $(window).width();
    if($w <= 768) {
       $('.main-footer').css({
          'width': '100%',
          'left': '0'
       });
    } else {
       var $_I = $('.places-content.places-all').width();
       var $__I = $('.places-content.places-all').find('.container').width();

       var $half = parseInt($_I) - parseInt($__I);
       $half = parseInt($half) / 2;

       $('.main-footer').css({
          'width': $_I+'px',
          'left': '-'+$half+'px'
       });
    }
  }
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');
	$('.places-content').removeClass().addClass('places-content '+$href);
	$this = $(this);
});

// js to initialize justified gallery
setTimeout(
function() 
{
  $('.home-justified-gallery').justifiedGallery({
    lastRow: 'nojustify',
    rowHeight: 220,
    maxRowHeight: 220,
    margins: 10,
    sizeRangeSuffixes: {
         lt100: '_t',
         lt240: '_m',
         lt320: '_n',
         lt500: '',
         lt640: '_z',
         lt1024: '_b'
    }
  });
  $('.home-justified-gallery').css('opacity', '1');
}, 8000);

/* place destination */
function placedest(placeid,place,type){
  $.ajax({
    type: 'POST',
    url: '?r=site/take-dest-action',
    data: 'place='+place+'&type='+type,
    success: function (data) {
      if(data != '0' || data != 0) {
        $("."+placeid+"_past").removeClass("active");
        $("."+placeid+"_future").removeClass("active");
        $("."+placeid+"_"+type).addClass("active");
        
        if(type == 'past') {
          Materialize.toast('Added to the been their list.', 2000, 'green');
        } else if (type == 'future') {
          Materialize.toast('Posted to the visit list.', 2000, 'green');
        }
      } else {
        if(type == 'past' || type == 'future') {
          Materialize.toast('Already Exist.', 2000, 'red');
        }
      }
    }
  });
}

/* back image */ 
function backimage() {
  $.ajax({
    type: 'POST',
    url: '?r=site/placesbanner',
    data: 'place='+place+'&baseUrl='+baseUrl+'&placetitle='+placetitle+'&placefirst='+placefirst,
    success: function(data) {
      $(".places-banner").css("background","url("+data+")");
      $(".places-banner").css("background-repeat","no-repeat");
      $(".places-banner").css("background-position","center center");
      $(".places-banner").css("background-size","cover");
      $("#place_explore").attr("src",data);
    }
  });
}


function displayplace(count,type,token) {
  if(type === 'attr')
  {
    displayatractions(count,type,token);
  } 
  else
  {
    $.ajax({
      type: 'POST',
      url: '?r=site/getplace'+type,
      beforeSend: function() {
        if(count === 3 || count === 4) {
          $("#popular-"+type+"-list").html($loader);
        } else {
          if(type === 'hotels'){
            $("#places-lodge").html($loader);
          }
          else if(type === 'rest'){
            $("#places-dine").html($loader);
          }
          else if(type === 'attr'){
            $("#places-todo").html($loader);
          } else {
            $("#places-"+type).html(data);
          }

        }
      },
      data: {
        place: place,
        baseUrl: baseUrl,
        count: count,
        placetitle: placetitle,
        placefirst: placefirst,
        token: token
      },
      success: function (data) {
        $('.loaderblock').remove();
        if(count === 3 || count === 4) {
          $("#popular-"+type+"-list").html(data);
        } else {
          if(type === 'hotels'){type = 'lodge';}
          if(type === 'rest'){type = 'dine';}
          if(type === 'attr'){type = 'todo';}
          $("#places-"+type).html(data);
          /* manage expandable -photo slider : hotels/places : more info */     
          $(".subtab-menu > li").click(function(e){
            initSubtabSlider(this);
          });
          /* end manage expandable -photo slider : hotels/places : more info */
          setTimeout(function() { 
                  initDropdown();
                  if($("#distance-slider").length>0)
              initSlidernew("distance-slider",0,100,1,'km');
            if($("#price-slider").length>0)
              initSlidernew("price-slider",500,10000,2500,'$');
              }, 400);
        }
        fixPlacesImageUI("all"); 
      }
    });
  }
}

/* get tour list particular */
function getTourlistparticular(city, name) {
  $('#placedesc').html('Attractions in '+city);
  rightplaceboxes();  
  $('.home').removeClass('active');
  document.body.scrollTop = 0; 
    document.documentElement.scrollTop = 0;    
  $.ajax({
        type: 'POST',
        url: '?r=site/gettourlistparticular',
        data: {name, city},
        success: function (data) {
          $("#places-todo").html(data);
      setTimeout(function(){
        initDropdown(); 
        $('.tabs').tabs();
        fixPlacesImageUI("all");
            //tabsNewInit('#places-todo');
      }, 500);

      $(".subtab-menu > li").click(function(e){
        initSubtabSlider(this);
      });
        }
    });
}

/* attraction pagination click */
function attractionsPaginationClick() {
  var $start = $('#popular-attractions-list').find('.row').find('.fpb-holder').length;
  if($start>=3) {
    $.ajax({
          url: '?r=site/getplaceattractions',
          success: function (data) {
            $('#popular-attractions-list').find('.row').html(data);
            $(".topattractions-section .cbox-title .top-stuff").remove();
            setTimeout(function(){
          initPhotoCarousel("topattractions");
          fixPlacesImageUI("all");
          $(".topattractions-section .carousel-albums").css("opacity",1);
        },500);
          }
      });
  }
}

/* near cities local */
function nearbycitieslocal($placefirst, $placetitle) {
  $('#placedesc').html('Locals of '+$placetitle);
  document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;   
  $.ajax({
        type: 'POST',
        url: '?r=site/getplacelocals',
        data: 'place='+$placefirst+'&baseUrl='+baseUrl+'&count=all&placetitle='+$placefirst+'&placefirst='+$placefirst,
        success: function (data) {
          $("#places-locals").html(data);
          $('.tablist').find('.text-menu').find('li.active').removeClass('active');
          $('.tablist').find('.text-menu').find('li.locals').addClass('active');
          $('.tab-content').find('div.tab-pane.active').removeClass('active in');
          $('.tab-content').find('#places-locals').addClass('active in');
          rightplaceboxes();
      $('#rp_locals').hide();
      }
    });
}

/* top places pagination click */
function topplacesPaginationClick() {
  var $start = $('#popular-topplaces-list').find('.row').find('.fpb-holder').length;
  if($start>=3) {
    $.ajax({
          url: '?r=site/getplacetopplaces',
          success: function (data) {
            $('#popular-topplaces-list').find('.row').html(data);
            $(".topplaces-section .cbox-title .top-stuff").remove();

            setTimeout(function(){
          initPhotoCarousel("topplaces");
          fixPlacesImageUI("all");
          $(".topplaces-section .carousel-albums").css("opacity",1);
        },500);
          }
      });
  }
}

/* get all item */
function getallitem(type, placetitle, placefirst, token, label) {
  if(type == 'hotels') {
    $('#placedesc').html('Hotels in '+placefirst);
    $('#rp_map').hide();
    $('#rp_travellers').hide();
    $('#rp_locals').hide();
    $('#rp_reviews').hide();
    $('#rp_tips').hide();
    $('#rp_booking').hide();
    $('#rp_recent_questions').hide();
    $('#rp_hotel_deal').hide();
    $('#rp_place_explore').hide();
    if($("#distance-slider").length>0)
      initSlidernew("distance-slider",0,100,1,'km');
    if($("#price-slider").length>0)
      initSlidernew("price-slider",500,10000,2500,'$');
  } else {
    $('#placedesc').html('Restaurants in '+placefirst);
  }
  $('.home').removeClass('active');
  document.body.scrollTop = 0; 
    document.documentElement.scrollTop = 0;   
    $.ajax({
        type: 'POST',
        url: '?r=site/getplace'+type,
        data: 'place='+placefirst+'&baseUrl='+baseUrl+'&count=all&placetitle='+placetitle+'&placefirst='+placefirst+'&token='+token,
        success: function (data) {
        $("#places-"+label).html(data);
      $(".new-post").click(function(){
        expandNewpost(this);
      });
        }
    });
}

/* manage map / list section */
function closeListSection(obj) {
  var sparent=$(obj).parents(".tcontent-holder");
  sparent.find(".places-content-holder").find(".list-holder").slideUp();
  $(obj).html('<i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>List');
  $(obj).attr("onclick","openListSection(this)");
}

function openListSection(obj) {
  var sparent=$(obj).parents(".tcontent-holder");
  sparent.find(".places-content-holder").find(".list-holder").slideDown();
  //if($('#places-todo:visible').length == 0)
  $(obj).html('<i class="mdi mdi-close"></i>Close List');
  $(obj).attr("onclick","closeListSection(this)");
}
/* end manage map / list section */

/* FUN manage expand map */
  function expandMap(obj,target) {    
    var sparent=$(obj).parents(".map-holder");
    sparent.find(".overlay").hide();
    sparent.find("a.closelink").show();
    sparent.addClass("expanded"); 
    var orgScroll = $('body').scrollTop()-30;
    if(($(target).offset().top)>100 && !$(obj).parents(".subtab").length>0) {
      $('body').animate({
        scrollTop: (eval($(target).offset().top-30))+orgScroll
      }, 1000);
    }
    $(".places-mapview").addClass("showMapView");
    setTimeout(function(){initNiceScroll(".places-mapview .nice-scroll");},500);
    $('body').animate({
      scrollTop:0
    }, 1000);
  }

  function shrinkMap(obj) {
    var sparent=$(obj).parents(".map-holder");
    sparent.find(".overlay").show();
    sparent.find("a.closelink").hide();
    sparent.removeClass("expanded");  
    if($(".places-mapview").hasClass("showMapView")){
      $(".places-mapview").removeClass("showMapView");
    }
  }
/* FUN end manage expand map */

/* display place */
function displayplace(count,type,token) {
  if(type === 'attr')
  {
    displayatractions(count,type,token);
  } 
  else
  {
    $.ajax({
      type: 'POST',
      url: '?r=site/getplace'+type,
      beforeSend: function() {
        if(count === 3 || count === 4) {
          $("#popular-"+type+"-list").html($loader);
        } else {
          if(type === 'hotels'){
            $("#places-lodge").html($loader);
          }
          else if(type === 'rest'){
            $("#places-dine").html($loader);
          }
          else if(type === 'attr'){
            $("#places-todo").html($loader);
          } else {
            $("#places-"+type).html(data);
          }

        }
      },
      data: {
        place: place,
        baseUrl: baseUrl,
        count: count,
        placetitle: placetitle,
        placefirst: placefirst,
        token: token
      },
      success: function (data) {
        $('.loaderblock').remove();
        if(count === 3 || count === 4) {
          $("#popular-"+type+"-list").html(data);
        } else {
          if(type === 'hotels'){type = 'lodge';}
          if(type === 'rest'){type = 'dine';}
          if(type === 'attr'){type = 'todo';}
          $("#places-"+type).html(data);
          /* manage expandable -photo slider : hotels/places : more info */     
          $(".subtab-menu > li").click(function(e){
            initSubtabSlider(this);
          });
          /* end manage expandable -photo slider : hotels/places : more info */
          setTimeout(function() { 
                  initDropdown();
                  if($("#distance-slider").length>0)
              initSlidernew("distance-slider",0,100,1,'km');
            if($("#price-slider").length>0)
              initSlidernew("price-slider",500,10000,2500,'$');
              }, 400);
        }
        fixPlacesImageUI("all"); 
      }
    });
  }
}  

function displayatractions(count,type,token,$lazyhelpcount=0) {
  $('#places-'+type).find('.lazyloadscrollattraction').removeClass('lazyloadscrollattraction');
  $.ajax({
    type: 'POST',
    url: '?r=site/getplace'+type,
    beforeSend: function() {
      if(count === 3 || count === 4) {
        $("#popular-"+type+"-list").append($loader);
      } else {
        if(type === 'attr') {
          $("#places-todo").append($loader);
        } else {
          $("#places-"+type).append($loader);
        }
      }
    },
    data: 'place='+place+'&baseUrl='+baseUrl+'&count='+count+'&placetitle='+placetitle+'&placefirst='+placefirst+'&token='+token+'&$lazyhelpcount='+$lazyhelpcount,
    success: function (data) {
      $('.loaderblock').remove();
      if(count === 3 || count === 4) {
        if($lazyhelpcount == '' || $lazyhelpcount == 0) {
          $("#popular-"+type+"-list").html(data);
        }
        else
        {
          $("#popular-"+type+"-list").append(data);
        } 
        postNumber = $lazyhelpcount + 1;
        $("#places-"+type).find('.attractionbox').last().addClass('lazyloadscrollattraction');
      } else {
        if(type === 'attr'){type = 'todo';}
        if($lazyhelpcount == '' || $lazyhelpcount == 0) {
          $("#places-"+type).html(data);    
        }
        else
        {
          $("#places-"+type).append(data);    
        } 
        postNumber = $lazyhelpcount + 1;
        $("#places-"+type).find('.attractionbox').last().addClass('lazyloadscrollattraction');
        $(".subtab-menu > li").click(function(e){
          initSubtabSlider(this);
        });
      }
      setTimeout(function() { 
              initDropdown();
          }, 400);
      fixPlacesImageUI("all"); 
    }
  });
  
}
  
/* START Lazy Loading */
window.onload = function() {
  $.fn.is_on_screen = function(){
    var win = $(window);
    var viewport = {
      top : win.scrollTop(),
      left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
     
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };
  if( $('.lazyloadscrollattraction').length > 0 ) { // if target element exists in DOM
    if( $('.lazyloadscrollattraction').is_on_screen() ) { // if target element is visible on screen after DOM loaded
      displayatractions(postNumber);
    }
  } 
    
  $('body').bind('touchmove', function(e) { 
    if( $('.lazyloadscrollattraction').length > 0 ) { // if target element exists in DOM
      if( $('.lazyloadscrollattraction').is_on_screen() ) { // if target element is visible on screen after DOM loaded
        displayatractions(postNumber);
      }
    }
  });
  $(window).scroll(function(){ // bind window scroll event 
    if( $('.lazyloadscrollattraction').length > 0 ) { // if target element exists in DOM
      if( $('.lazyloadscrollattraction').is_on_screen() ) { // if target element is visible on screen after DOM loaded
        displayatractions(postNumber);
      }
    }
  });
}

/* listing hotels */
function gethotellist() {
  if(placetitle != undefined && placetitle != null && placetitle != '') {
    $.ajax({
      type: 'POST',
      url: '?r=site/get-hotel-list',
      data: {placetitle},
      success: function(data) {
        $('#rp_hotel_deal').find('.cbox-desc').html(data);
      }
    });
  }
}

/* right place boxes */
function rightplaceboxes() {
  $('#rp_map').show();
  $('#rp_travellers').show();
  $('#rp_locals').show();
  $('#rp_reviews').show();
  $('#rp_tips').show();
  $('#rp_booking').show();
  $('#rp_recent_questions').show();
  $('#rp_hotel_deal').show();
  $('#rp_place_explore').show();
}

/* FUN set hide header for mobile */
function setHideHeader(obj,pagename,doWhat) {
  var win_w=$(window).width();    
  if(pagename=="places"){
    var whichTab=$(obj).find("a").attr("href");
    if(whichTab!="#places-all") {
      if(win_w<=767){
        $('body').addClass("hideHeader");
      } else {
        if($('body').hasClass("hideHeader")){
          $('body').removeClass("hideHeader");
        }
      } 
    } else {
      if($('body').hasClass("hideHeader")){
        $('body').removeClass("hideHeader");
      }
    }
    var dropVal=whichTab.replace("#places-","");
    $(".select2places").val(dropVal).change();
    setTimeout(function(){fixPlacesImageUI(dropVal);},400);     
    resetTagging();
  }
  if(pagename=="hotels") {
      if(win_w<=767) {
      if(doWhat == "hide") $('body').addClass("hideHeader");
      else $('body').removeClass("hideHeader");
    } else {
      if($('body').hasClass("hideHeader")) {
        $('body').removeClass("hideHeader");
      }
    }
  }
}
/* FUN end set hide header for mobile */

function resetTagging() {
  $(".places-page .tagging").each(function() {
    if($(this).hasClass("expandTags")) {
      $(this).removeClass("expandTags");
    }
  });
}

/* places page - new post popup OPEN */
  function openPlacesNewPost() {
    var selected = $(".nav-tabs.text-menu > li.active a").html().trim().toLowerCase();
    if(selected == "ask") {
    } else if(selected == "tips") {
    } else if(selected == "reviews") {
    } else {
    }
    setTimeout(function(){fixImageUI();setGeneralThings();},400);
  }
/* end places page - new post popup OPEN */