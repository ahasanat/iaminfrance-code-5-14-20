<?php

class HTMLPurifier_HTMLModule_TargetBlankTest extends HTMLPurifier_HTMLModuleHarness
{

    public function setUp()
    {
        parent::setUp();
        $this->config->set('HTML.TargetBlank', true);
    }

    public function testTargetBlank()
    {
        $this->assertResult(
            '<a href="https://google.com">a</a><a href="/local">b</a><a href="mailto:foo@example.com">c</a>',
            '<a href="https://google.com" target="_blank">a</a><a href="/local">b</a><a href="mailto:foo@example.com">c</a>'
        );
    }

}

// vim: et sw=4 sts=4
