<?php

class HTMLPurifier_AttrDef_CSS_URITest extends HTMLPurifier_AttrDefHarness
{

    public function test()
    {
        $this->def = new HTMLPurifier_AttrDef_CSS_URI();

        $this->assertDef('', false);

        // we could be nice but we won't be
        $this->assertDef('https://www.example.com/', false);

        $this->assertDef('url(', false);
        $this->assertDef('url("")', true);
        $result = 'url("https://www.example.com/")';
        $this->assertDef('url(https://www.example.com/)', $result);
        $this->assertDef('url("https://www.example.com/")', $result);
        $this->assertDef("url('https://www.example.com/')", $result);
        $this->assertDef(
            '  url(  "https://www.example.com/" )   ', $result);
        $this->assertDef("url(https://www.example.com/foo,bar\)\'\()",
            'url("https://www.example.com/foo,bar%29%27%28")');
    }

}

// vim: et sw=4 sts=4
