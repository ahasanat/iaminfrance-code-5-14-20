<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => '34.217.21.181',
                'port' => 6379,
                'database' => 0,
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'enableCsrfValidation'=>false,
        ],
        'EphocTime' => [
 
            'class' => 'common\components\EphocTime',
 
            ],
        'GenCls' => [
 
            'class' => 'common\components\GenCls',
 
            ],
         'CDbCriteria' => [
 
            'class' => 'common\components\CDbCriteria',
 
            ],
       'assetManager' => [
       // 'linkAssets' => true,
	    'linkAssets' => false,
        ], 
        // Fcaebook App Details
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '447974302499180',                
                    'clientSecret' => 'cb057b32245bf80df4d4a1cc6c701aa4',
                ],
                'google' => [ 
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'returnUrl' => 'https://iaminfrance.com/frontend/web/index.php?r=google%2Fauth&authclient=google',
                    'clientId' => '177791802391-elblp95c43p5plr9qq5pv6a3gn4nq48n.apps.googleusercontent.com',
                    'clientSecret' => 'pEgDX7MSu46n0rqWxVuCmBrO',
                ],
                // below for local
                /*'google' => [  // for local
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'returnUrl' => 'http://localhost/iaminfrance-code/frontend/web/index.php?r=site%2Fauth&authclient=google',
                    'clientId' => '22165896532-mir5q1glji7d00f0v8ksn523nbmh5npi.apps.googleusercontent.com',
                    'clientSecret' => 'ovWy0F54ZBM77qv9bsUueIPd',
                ],*/
            ],
         ],
         'i18n' => [
            'translations' => [
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
        //google+ login settings start
         'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
                'google' => [
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'Google',
                ],
                        //google+ login settings start
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
         'session' => [
            'class' => 'yii\mongodb\Session',
            'timeout' => 3600 * 72,
            'name' => 'PHPFRONTSESSID',
            'savePath' => __DIR__ . '/../tmp',
        ],
    ],   
    'params' => $params,
];
