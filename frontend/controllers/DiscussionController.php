<?php
namespace frontend\controllers;
use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\mongodb\ActiveRecord;
use frontend\models\LoginForm;
use frontend\models\PostForm;
use frontend\models\Personalinfo;
use frontend\models\UserForm;
use frontend\models\Vip; 
use frontend\models\TravAdsVisitors;
use frontend\models\Order;
use frontend\models\UserMoney;
use frontend\models\PlaceDiscussion;
use frontend\models\Notification;
use frontend\models\Destination;
use frontend\models\PlaceVisitor;
use frontend\models\SecuritySetting;
use frontend\models\UserPhotos;
use backend\models\DefaultPosts;
use frontend\models\PinImage;
use frontend\models\General;
class DiscussionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
      
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
                'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];           
    }
    
    public function actionIndex()  
    {
    	$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id); 
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }

        $place = Yii::$app->params['place'];
        $placetitle = Yii::$app->params['placetitle'];
        $placefirst = Yii::$app->params['placefirst'];
        $lat = Yii::$app->params['lat'];
        $lng = Yii::$app->params['lng'];
        
        $getpplacereviews = PlaceDiscussion::getPlaceReviews($place,'discussion','all'); 
        
        return $this->render('index',array('checkuserauthclass' => $checkuserauthclass,'place' => $place,'placetitle' => $placetitle,'placefirst' => $placefirst,'getpplacereviews'=> $getpplacereviews,'lat' => $lat,'lng' => $lng));
    }

    public function actionComposenewdiscussionpopup() 
    {
        return $this->render('adddiscussion');
    }

    public function actionAddDiscussionPlaces()
    {
        $this->layout = 'ajax_layout';
        $session = Yii::$app->session;
        $email = $session->get('email');
        $userid = $user_id = (string)$session->get('user_id');
        
        if(isset($userid) && $userid != '') {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') {
                $data['auth'] = $authstatus;
                return $authstatus;
            } else {
                $result = LoginForm::find()->where(['_id' => $userid])->one();
                $date = time();
                $post_status = '0';
                $text = isset($_POST['test']) ? $_POST['test'] : '';
                
                $purifier = new HtmlPurifier();
                $text = HtmlPurifier::process($text);
                $post = new PlaceDiscussion();
                $post->post_status = '1';
                $post->post_text = ucfirst($text);
                $post->post_type = 'text';
                $post->post_created_date = "$date";
                $post->post_user_id = "$user_id";

                if(isset($_POST['current_location']) && !empty($_POST['current_location']) && $_POST['current_location']!='undefined')
                {
                    $post->currentlocation = $_POST['current_location'];
                }

                $post->custom_share = (isset($_POST['sharewith']) && !empty($_POST['sharewith'])) ? $_POST['sharewith'] : '';
                $post->custom_notshare = (isset($_POST['sharenot']) && !empty($_POST['sharenot'])) ? $_POST['sharenot'] : '';
                $post->anyone_tag = (isset($_POST['customchk']) && !empty($_POST['customchk'])) ? $_POST['customchk'] : '';
                $post->post_tags = (isset($_POST['posttags']) && !empty($_POST['posttags'])) ? $_POST['posttags'] : '';    
                $post->post_title = isset($_POST['title']) ? ucfirst($_POST['title']) : '';

                $post->post_title = ucfirst($_POST['title']);
                $post->comment_setting = $_POST['comment_setting'];
                $post->post_privacy = $_POST['post_privacy'];
                $post->customids = $_POST['custom'];
                $post->is_deleted = "$post_status";
                $post->post_ip = $_SERVER['REMOTE_ADDR'];
                $post->placetype = 'discussion';

            
                $img = '';
                $im = '';
                $url = '../web/uploads/';
                $urls = '/uploads/';

                $imageBulkArray = array();


                if (isset($_FILES['imageFile1']) && count($_FILES["imageFile1"]["name"]) >0) 
                {
                    $imgcount = count($_FILES["imageFile1"]["name"]);
                    for ($i =0; $i < $imgcount; $i++)
                    {
                        if (isset($_FILES["imageFile1"]["name"][$i]) && $_FILES["imageFile1"]["name"][$i] != "") 
                        {
                            if ($text == '') { $post->post_type = 'image'; }
                            else { $post->post_type = 'text and image'; }
                            
                            $image_extn = end(explode('.',$_FILES["imageFile1"]["name"][$i]));
                            $rand = rand(111,999).'_'.time();
                            move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);
                            
                            $img = $urls.$date.$rand.'.'.$image_extn;
                            $imageBulkArray[] = $img;
                        }
                    }
                }

                if (isset($_POST['imageFile2']) && count($_POST["imageFile2"]) >0) 
                {
                    $imageFile2 = array_values($_POST['imageFile2']);

                    foreach ($imageFile2 as $simageFile2) {
                        if(file_exists($simageFile2)) {
                            
                            if ($text == '') { $post->post_type = 'image'; }
                            else { $post->post_type = 'text and image'; }
                            
                            $image_extn = end(explode('.', $simageFile2));
                            $rand = rand(111,999).'_'.time();
                            $newname = $url.$date.$rand.'.'.$image_extn;

                            copy($simageFile2, $newname);
                            
                            $newname = str_replace('../web/uploads/', '/uploads/', $newname);
                            $img = $newname;
                            $imageBulkArray[] = $img;

                        }       
                    }
                }

                if(!empty($imageBulkArray)) {
                    $imageBulkArray = implode(',', $imageBulkArray);
                    $imageBulkArray = $imageBulkArray.',';
                    $post->image = $imageBulkArray;
                } else {
                    $post->image = '';
                }

                $post->insert();

                $last_insert_id =  $post->_id;
                
                // Insert record in notification table also
                $notification = new Notification();
                $notification->post_id = "$last_insert_id";
                $notification->user_id = "$user_id";
                $notification->notification_type = 'post';
                $notification->is_deleted = '0';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $notification->insert();
                
                if(isset($_POST['posttags']) && $_POST['posttags'] != 'null')
                {
                    // Insert record in notification table also
                    $tag_connections = explode(',',$_POST['posttags']);
                    $tag_count = count($tag_connections);
                    for ($i = 0; $i < $tag_count; $i++)
                    {
                        $result_security = SecuritySetting::find()->where(['user_id' => "$tag_connections[$i]"])->one();
                        if ($result_security)
                        {
                            $tag_review_setting = $result_security['review_posts'];
                        }
                        else
                        {
                            $tag_review_setting = 'Disabled';
                        }
                        $notification =  new Notification();
                        $notification->post_id =   "$last_insert_id";
                        $notification->user_id = $tag_connections[$i];
                        $notification->notification_type = 'tag_connect';
                        $notification->review_setting = $tag_review_setting;
                        $notification->is_deleted = '0';
                        $notification->status = '1';
                        $notification->created_date = "$date";
                        $notification->updated_date = "$date";
                        $notification->insert();
                    }
                }
                if($post_status == '0')
                {
                    $this->display_last_discussion($last_insert_id);
                }
                else
                {
                    if($page_details['gen_post_review'] == 'on' && $page_details['created_by'] != $user_id)
                    {
                        $this->display_review_message();
                    }
                    else
                    {
                        $this->display_last_discussion($last_insert_id);
                    }
                }
            }
        } else {
            return 'checkuserauthclassg';
        }
    }

    public function actionDeleteDiscussion() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            return PlaceDiscussion::DeleteDiscussionCleanUp($_POST['pid'],$_POST['post_user_id']);
        }
    }

    public function actionEditDiscussion() 
    {
        $pid = isset($_POST['pid']) ? $_POST['pid'] : '';
        if ($pid != '') {
            $date = time(); 
            $update = PlaceDiscussion::find()->where(['_id' => $pid])->andWhere(['not','flagger', "yes"])->one();
            $text = $update['post_text'];
            $image = $update['image'];

            if(isset($_POST['current_location']) && !empty($_POST['current_location']) && $_POST['current_location']!='undefined')
            {
                $update->currentlocation = $_POST['current_location'];
            }
            $update->post_created_date = "$date";
            
            $update->custom_share = (isset($_POST['sharewith']) && !empty($_POST['sharewith'])) ? $_POST['sharewith'] : '';
            $update->custom_notshare = (isset($_POST['sharenot']) && !empty($_POST['sharenot'])) ? $_POST['sharenot'] : '';
            $update->anyone_tag = (isset($_POST['customchk']) && !empty($_POST['customchk'])) ? $_POST['customchk'] : '';
            $update->post_tags = (isset($_POST['posttags']) && !empty($_POST['posttags'])) ? $_POST['posttags'] : '';

            $update->post_title = ucfirst($_POST['title']);
            $update->comment_setting = $_POST['comment_setting'];
            $update->post_privacy = $_POST['post_privacy'];
            $update->customids = $_POST['custom'];
            
            $img = '';
            $im = '';
            $url = '../web/uploads/';
            $urls = '/uploads/';

            $imageBulkArray = array();

            if (isset($_FILES['imageFile1']) && count($_FILES["imageFile1"]["name"]) >0) 
            {
                $imgcount = count($_FILES["imageFile1"]["name"]);
                for ($i = 0; $i < $imgcount; $i++)
                {
                    if (isset($_FILES["imageFile1"]["name"][$i]) && $_FILES["imageFile1"]["name"][$i] != "") 
                    {
                        $image_extn = end(explode('.',$_FILES["imageFile1"]["name"][$i]));
                        $rand = rand(111,999).'_'.time();

                        move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);
                        
                        $img = $urls.$date.$rand.'.'.$image_extn;
                        $imageBulkArray[] = $img;
                    }
                }
            }

            if (isset($_POST['imageFile2']) && count($_POST["imageFile2"]) >0) 
            {
                $imageFile2 = array_values($_POST['imageFile2']);

                foreach ($imageFile2 as $simageFile2) {
                    if(file_exists($simageFile2)) {
                        $image_extn = end(explode('.', $simageFile2));
                        $rand = rand(111,999).'_'.time();
                        $newname = $url.$date.$rand.'.'.$image_extn;

                        copy($simageFile2, $newname);
                        
                        $newname = str_replace('../web/uploads/', '/uploads/', $newname);
                        $img = $newname;
                        $imageBulkArray[] = $img;

                    }       
                }
            }

            if(!empty($imageBulkArray)) {
                $existFiles = explode(',', $image);
                $files = array_merge($existFiles, $imageBulkArray);
                $files = array_values(array_filter($files));
                $files = implode(',', $files);
                if(!empty($files)) {
                    $files = $files.',';
                    $update->image = $files;
                } else {
                    $update->image = '';
                }
            } else {
                $files = explode(',', $image);
                $files = array_values(array_filter($files));
                $files = implode(',', $files);
                if(!empty($files)) {
                    $files = $files.',';
                    $update->image = $files;
                } else {
                    $update->image = '';
                }
            }

            if (trim($_POST['link_description']) != '' && trim($_POST['link_description']) != 'undefined')
            {
                $title = $_POST['link_title'];
                $description = $_POST['link_description'];
                $image = $_POST['link_image'];
                $url = $_POST['link_url'];

                $update->post_type = 'link';
                $update->link_title = ucfirst($title);
                $update->image = $image;
                $update->post_text = ucfirst($url);
                $update->link_description = $description;
            } else {
                if (isset($_POST['test']) && !empty($_POST['test'])) {
                    $update->post_text = ucfirst($_POST['test']);
                    
                    if ((isset($image) && $image != '') || !empty($imageBulkArray)) {
                        $update->post_type = 'text and image';
                    } else {   
                        $update->post_type = 'text';
                    } 
                }
            }

            $update->update();

            $last_insert_id = $pid;
            $this->display_last_discussion($last_insert_id);
            
            if($update['is_deleted'] == '2') {
                $post_flager_id = $update['post_flager_id'];
                
                /* Insert Notification For The Owner of Post For Flagging*/
                $notification =  new Notification();
                $notification->post_id = "$pid";
                $notification->user_id = "$post_flager_id";
                $notification->notification_type = 'editpostuser';
                $notification->is_deleted = '0';
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $notification->insert();
            }   
        } else {
            return "0";
        }
    }

    public function actionEditPostPreSetDiscussion()
    {
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        if(isset($userid) && $userid != '') {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') {
                $data['auth'] = $authstatus;
                return $authstatus;
            } else {
                $postid = isset($_POST['editpostid']) ? $_POST['editpostid'] : '';
                $post = PlaceDiscussion::find()->where(['_id' => $postid])->andWhere(['not','flagger', "yes"])->one();
                return $this->render('editdiscussion', array('post' => $post));
            }
        } else {
            return 'checkuserauthclassg'; 
        }
    }

    public function getdiscussiondefault()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $eximgs = array();
        $totalimgs = '';

        $UserPhotos = General::find()->where(["label" => "photostreamcount"])->asarray()->one();
        if(!empty($UserPhotos)) {
            if(isset($UserPhotos['images'])) {
                $eximgs = $UserPhotos['images'];
                $totalimgs = $UserPhotos['count'];
            }            
        }

        $DefaultPosts = DefaultPosts::find()->where(['module' => 'discussion'])->asarray()->one();
        if(!empty($DefaultPosts)) {
            if(!empty($eximgs)) {
                $title = isset($DefaultPosts['title']) ? $DefaultPosts['title'] : '';
                $description = isset($DefaultPosts['title']) ? $DefaultPosts['description'] : '';
                $imgcountcls="";
                if($totalimgs == 1){$imgcountcls = 'one-img';}
                if($totalimgs == 2){$imgcountcls = 'two-img';}
                if($totalimgs == 3){$imgcountcls = 'three-img';}
                if($totalimgs == 4){$imgcountcls = 'four-img';}
                if($totalimgs == 5){$imgcountcls = 'five-img';}
                if($totalimgs > 6){$imgcountcls = 'more-img';}
                ?>
                <div class="post-holder bborder tippost-holder defaultpost">
                   <div class="post-content">
                      <div class="post-details">
                         <div class="post-title"><h5><b><?=$title?></b></h5></div>
                         <div class="post-desc">
                            <?php if(strlen($description)>187){ ?>
                                <div class="para-section">
                                    <div class="para">
                                        <p><?=$description?></p>
                                    </div>
                                    <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                </div>
                            <?php }else{ ?>                                     
                                <p><?= $description?></p>
                            <?php } ?>
                         </div>
                      </div>
                      <div class="post-img-holder">
                        <div class="post-img two-img defaultpost-gallery <?= $imgcountcls?> gallery swipe-gallery">
                            <?php
                            $cnt = 1;
                            foreach ($eximgs as $eximg) {

                            if (file_exists('../web/'.$eximg)) {
                            $picsize = '';
                            $val = getimagesize('../web'.$eximg);
                            $picsize .= $val[0] .'x'. $val[1] .', ';
                            $iname = $this->getimagename($eximg);
                            $inameclass = $this->getimagefilename($eximg);
                            $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                            
                            if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                <a href="javascript:void(0)" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>" onclick="defaultimagesgallery()">
                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                    <?php if($cnt == 5 && $totalimgs > 5){?>
                                        <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                    <?php } ?>
                                </a>
                            <?php } $cnt++; } ?>
                        </div>
                        <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                            if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                            <?php } else { 
                                $iname = $this->getimagename($eximg);
                                $inameclass = $this->getimagefilename($eximg);
                                $image = '../web/uploads/gallery/'.$iname;
                                $JIDSsdsa = '';           
                                $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                if(!empty($pinit)) {
                                    $JIDSsdsa = 'active';                    
                                }
                                ?>
                                <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                            <?php 
                            } 
                        } 
                        ?>
                    </div>
                   </div>
                   <div class="clear"></div>
                </div>
                <?php
            }
        }
    }

    public function actionDefaultpostslides()
    {
        $UserPhotos = ArrayHelper::map(UserPhotos::find()->select(['image'])->asarray()->all(), function($data){return (string)$data['_id'];}, 'image');
        $eximgs = array();
        if(!empty($UserPhotos)) {
            foreach ($UserPhotos as $S_UserPhotos) {
                $c_images = explode(',', $S_UserPhotos);
                $eximgs = array_merge($eximgs, $c_images);
            }
        }
        $eximgs = array_values(array_filter($eximgs));
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        $cnt = 1;

        $result = array();
        foreach ($eximgs as $eximg) {
            if (file_exists('../web/'.$eximg)) {
                $getBaseUrl = Yii::$app->getUrlManager()->getBaseUrl();
                $src = $getBaseUrl.$eximg;
                $cur = array("src" => $src, "thumb" => $src);
                $result[] = $cur;
            }
        } 

        return json_encode($result, TRUE);
    }
}
?>